<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Home Routes
|--------------------------------------------------------------------------
*/

Auth::routes();

Route::get('/', function () {
    return view('home');
});

/*
|--------------------------------------------------------------------------
| Head Offices Routes
|--------------------------------------------------------------------------
*/

Route::get('spk', function () {
    return view('modules.spk.data');
});

Route::get('spk/allspk', function () {
    return view('modules.spk.data_all');
});

Route::get('spk', function () {
    return view('modules.spk.data');
});

Route::get('aksesoris', function () {
    return view('modules.aksesoris.data');
});

Route::get('pelanggan', function () {
    return view('modules.pelanggan.data');
});

Route::get('leasing', function () {
    return view('modules.leashing.data');
});

/*
|--------------------------------------------------------------------------
| Inventory Routes
|--------------------------------------------------------------------------
*/

Route::get('kendaraan', function () {
    return view('modules.kendaraan.data');
});


Route::get('bbn', function () {
    return view('modules.bbn.data');
});

Route::get('gudang', function () {
    return view('modules.gudang.data');
});

/*
|--------------------------------------------------------------------------
| Inventory Routes
|--------------------------------------------------------------------------
*/

Route::get('pengaturan/nospk', function () {
    return view('modules.pengaturan.data_nospk');
});

/*
|--------------------------------------------------------------------------
| Pembelian Routes
|--------------------------------------------------------------------------
*/

Route::get('pembelian', function () {
    return view('modules.pembelian.data');
});
/*
|--------------------------------------------------------------------------
| Web APIs
|--------------------------------------------------------------------------
|
| Application program interface (API) is a set of routines, protocols, and 
| tools for building software applications. An API specifies how software 
| components should interact. Additionally, APIs are used when programming 
| graphical user interface (GUI) components. A good API makes it easier to 
| develop a program by providing all the building blocks. A programmer 
| then puts the blocks together.
| 
*/

/*
|--------------------------------------------------------------------------
| Aksesoris APIs
|--------------------------------------------------------------------------
*/

Route::get('api/aksesoris', ['middleware' => 'cors','uses'=>'ApiAksesorisController@index']);
Route::post('api/aksesoris', ['middleware' => 'cors','uses'=>'ApiAksesorisController@store']);
Route::put('api/aksesoris', ['middleware' => 'cors','uses'=>'ApiAksesorisController@update']);
Route::delete('api/aksesoris', ['middleware' => 'cors','uses'=>'ApiAksesorisController@destroy']);

/*
|--------------------------------------------------------------------------
| Asuransi APIs
|--------------------------------------------------------------------------
*/

Route::get('api/asuransi', ['middleware' => 'cors','uses'=>'ApiAsuransiController@asuransi_read']);
Route::post('api/asuransi', ['middleware' => 'cors','uses'=>'ApiAsuransiController@add_asuransi']);
Route::put('api/asuransi', ['middleware' => 'cors','uses'=>'ApiAsuransiController@update_asuransi']);
Route::delete('api/asuransi', ['middleware' => 'cors','uses'=>'ApiAsuransiController@destroy_asuransi']);

/*
|--------------------------------------------------------------------------
| Biro APIs
|--------------------------------------------------------------------------
*/

Route::get('api/biro', ['middleware' => 'cors','uses'=>'ApiBiroController@index']);
Route::post('api/biro', ['middleware' => 'cors','uses'=>'ApiBiroController@store']);
Route::put('api/biro', ['middleware' => 'cors','uses'=>'ApiBiroController@update']);
Route::delete('api/biro', ['middleware' => 'cors','uses'=>'ApiBiroController@destroy']);

/*
|--------------------------------------------------------------------------
| CMO APIs
|--------------------------------------------------------------------------
*/

Route::get('api/cmo', ['middleware' => 'cors','uses'=>'ApiCMOController@index']);
Route::post('api/cmo', ['middleware' => 'cors','uses'=>'ApiCMOController@store']);
Route::put('api/cmo', ['middleware' => 'cors','uses'=>'ApiCMOController@update']);
Route::delete('api/cmo', ['middleware' => 'cors','uses'=>'ApiCMOController@destroy']);

/*
|--------------------------------------------------------------------------
| Ekspedisi APIs
|--------------------------------------------------------------------------
*/

Route::get('api/ekspedisi', ['middleware' => 'cors','uses'=>'ApiEkspedisiController@index']);
Route::post('api/ekspedisi', ['middleware' => 'cors','uses'=>'ApiEkspedisiController@store']);
Route::put('api/ekspedisi', ['middleware' => 'cors','uses'=>'ApiEkspedisiController@update']);
Route::delete('api/ekspedisi', ['middleware' => 'cors','uses'=>'ApiEkspedisiController@destroy']);

/*
|--------------------------------------------------------------------------
| Gudang APIs
|--------------------------------------------------------------------------
*/

Route::get('api/gudang', ['middleware' => 'cors','uses'=>'ApiGudangController@index']);
Route::post('api/gudang', ['middleware' => 'cors','uses'=>'ApiGudangController@store']);
Route::put('api/gudang', ['middleware' => 'cors','uses'=>'ApiGudangController@update']);
Route::delete('api/gudang', ['middleware' => 'cors','uses'=>'ApiGudangController@destroy']);

/*
|--------------------------------------------------------------------------
| Kasryawan APIs
|--------------------------------------------------------------------------
*/
Route::get('api/karyawan/sales', ['middleware' => 'cors','uses'=>'ApiKaryawanController@sales_view']);

/*
|--------------------------------------------------------------------------
| Kendaraan BBN APIs
|--------------------------------------------------------------------------
|
*/

Route::get('api/kendaraan/bbn', ['middleware' => 'cors','uses'=>'ApiKendaraanController@bbn_master']);
Route::put('api/kendaraan/bbn', ['middleware' => 'cors','uses'=>'ApiKendaraanController@bbn_update']);

/*
|--------------------------------------------------------------------------
| Kendaraan Type APIs
|--------------------------------------------------------------------------
*/

Route::get('api/kendaraan/type', ['middleware' => 'cors','uses'=>'ApiKendaraanController@type_read']);
Route::post('api/kendaraan/type', ['middleware' => 'cors','uses'=>'ApiKendaraanController@type_store']);
Route::put('api/kendaraan/type', ['middleware' => 'cors','uses'=>'ApiKendaraanController@type_update']);
Route::delete('api/kendaraan/type', ['middleware' => 'cors','uses'=>'ApiKendaraanController@type_destroy']);
Route::get('api/kendaraan/type/{id}', ['middleware' => 'cors','uses'=>'ApiKendaraanController@type_read']);

/*
|--------------------------------------------------------------------------
| Kendaraan Variant APIs
|--------------------------------------------------------------------------
*/

Route::get('api/kendaraan/variant', ['middleware' => 'cors','uses'=>'ApiKendaraanController@variant_read']);
Route::post('api/kendaraan/variant', ['middleware' => 'cors','uses'=>'ApiKendaraanController@variant_store']);
Route::put('api/kendaraan/variant', ['middleware' => 'cors','uses'=>'ApiKendaraanController@variant_update']);
Route::delete('api/kendaraan/variant', ['middleware' => 'cors','uses'=>'ApiKendaraanController@variant_destroy']);
Route::get('api/kendaraan/variant/{id}', ['middleware' => 'cors','uses'=>'ApiKendaraanController@variant_type']);

/*
|--------------------------------------------------------------------------
| Leasing APIs
|--------------------------------------------------------------------------
*/

Route::get('api/leasing', ['middleware' => 'cors','uses'=>'ApiLeasingController@index']);
Route::post('api/leasing', ['middleware' => 'cors','uses'=>'ApiLeasingController@store']);
Route::put('api/leasing', ['middleware' => 'cors','uses'=>'ApiLeasingController@update']);
Route::delete('api/leasing', ['middleware' => 'cors','uses'=>'ApiLeasingController@destroy']);

/*
|--------------------------------------------------------------------------
| No SPK APIs
|--------------------------------------------------------------------------
*/

Route::get('api/pengaturan/nospk', ['middleware' => 'cors','uses'=>'ApiNospkController@index']);
Route::post('api/pengaturan/nospk', ['middleware' => 'cors','uses'=>'ApiNospkController@store']);
Route::put('api/pengaturan/nospk', ['middleware' => 'cors','uses'=>'ApiNospkController@update']);
Route::delete('api/pengaturan/nospk', ['middleware' => 'cors','uses'=>'ApiNospkController@destroy']);

/*
|--------------------------------------------------------------------------
| Pembelian APIs
|--------------------------------------------------------------------------
*/

Route::get('api/pembelian/new', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@baru']);
Route::get('api/pembelian', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@index']);
Route::post('api/pembelian/import', 'ApiTrKendaraanController@import');
Route::post('api/pembelian', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@store']);
Route::put('api/pembelian', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@update']);
Route::delete('api/pembelian', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@destroy']);

/*
|--------------------------------------------------------------------------
| Pelanggan APIs
|--------------------------------------------------------------------------
*/

Route::get('api/pelanggan', ['middleware' => 'cors','uses'=>'ApiPelangganController@index']);
Route::delete('api/pelanggan', ['middleware' => 'cors','uses'=>'ApiPelangganController@destroy']);
Route::get('api/pelanggan/{id}', ['middleware' => 'cors','uses'=>'ApiPelangganController@detail']);

/*
|--------------------------------------------------------------------------
| SPK APIs
|--------------------------------------------------------------------------
*/

Route::get('api/spk', ['middleware' => 'cors','uses'=>'ApiSPKController@index']);
Route::get('api/spk/{id}', ['middleware' => 'cors','uses'=>'ApiSPKController@detail']);


/*
|--------------------------------------------------------------------------
| VendorAks APIs
|--------------------------------------------------------------------------
*/

Route::get('api/vendorAks', ['middleware' => 'cors','uses'=>'ApiVendorAksController@index']);
Route::post('api/vendorAks', ['middleware' => 'cors','uses'=>'ApiVendorAksController@store']);
Route::put('api/vendorAks', ['middleware' => 'cors','uses'=>'ApiVendorAksController@update']);
Route::delete('api/vendorAks', ['middleware' => 'cors','uses'=>'ApiVendorAksController@destroy']);

/*
|--------------------------------------------------------------------------
| Warna APIs
|--------------------------------------------------------------------------
*/

Route::get('api/warna', ['middleware' => 'cors','uses'=>'ApiWarnaController@index']);
Route::get('api/warna/variant', ['middleware' => 'cors','uses'=>'ApiWarnaController@warna_variant']);
Route::post('api/warna', ['middleware' => 'cors','uses'=>'ApiWarnaController@store']);
Route::put('api/warna', ['middleware' => 'cors','uses'=>'ApiWarnaController@update']);
Route::delete('api/warna', ['middleware' => 'cors','uses'=>'ApiWarnaController@destroy']);


/*
|--------------------------------------------------------------------------
| Undefined Routes and APIs
|--------------------------------------------------------------------------
*/

Route::get('/import', function () {
    return view('import');
});

Route::post('/import', ['middleware' => 'cors', 'uses'=>'ImportController@store']);
Route::resource('import', 'ImportController');