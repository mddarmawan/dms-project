@extends('layout')

@section('content')

	<div class="content-header">
		<h6>
			<small>Penjualan</small>
			Monitoring SPK
		</h6>
		<ul class="header-tools right">
            <li><a href="{{url('spk')}}" class="chip">Monitoring SPK</a></li>
			<li><a href="{{url('spk/summary')}}" class="chip">Summary SPK</a></li>
			<li><a href="{{url('spk/tracking')}}" class="chip">SPK Tracking</a></li>
			<li><a href="{{url('spk/composition')}}" class="chip">Sales Composition</a></li>
			<li><a href="{{url('spk/cancel')}}" class="chip">SPK Cancel</a></li>
			<li><a href="{{url('spk/reqfaktur')}}" class="chip">Request Cetak</a></li>
			<li><a href="{{url('spk/allspk')}}" class="chip active">All SPK</a></li>
		</ul>
	</div>

	<div class="wrapper">
		<div id="allSPK">
 		
		</div>
		<div class="wrapper" style="position: absolute;top:470px;left: 1400px">
			 <a href="#tambah" class="btn-floating btn-large waves-effect waves-light red tambah"><i class="fa fa-plus left"></i></a>
		</div>
	</div>
	
	<div id="tambah" class="modal modal-fixed-footer" style="width:900px; height: auto;">
	    <form method="POST" action="{{url('/leashing')}}">
	    	<input type="hidden" id="state" name="state">
	        <h6 class="modal-title blue-grey darken-1">
	            <span id="form_title">TAMBAH SPK BARU</span>
	            <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
	        </h6>
	        <ul class="tabs blue-grey darken-1 white-text">
		        <li class="tab col s3"><a class="active" href="#pemesan" >Pemesan</a></li>
		        <li class="tab col s3"><a href="#stnk">Stnk</a></li>
		        <li class="tab col s3"><a href="#kendaraan">Kendaraan & Diskon</a></li>
		        <li class="tab col s3"><a href="#pembayaran">Pembayaran</a></li>
		     </ul>

	        <div class="modal-content" style="padding:10px;position: relative;">
	            @include("modules.spk.tambah")
	        </div>
	          <div style="padding:10px;text-align:right;background:#f5f5f5">
	            <a class="waves-effect waves-light btn save"><i class="material-icons left">save</i> <span id="btn_save"> Save</span></a>
	        </div>
	     </form>
	</div>


	<script>
	var total_bayar=0;
	var piutang = 0;
	var total_aksesoris = 0;
	var total_diskon = 0;
	var total_leasing = 0;
	var hpp=0;
	var bbn=0;
	var off=0;
	var dpp=0;
	var ppn=0;
	function detail(e){
		var id = $(e).attr("data-id");
		$("#detail a[href='#pemesan").trigger("click");
		$("#spk_id").html(id);
		$("#msg").html('');

		$.ajax({
            type: "GET",
            url: "{{url('api/spk/all')}}/"+id
        }).done(function(json) {
		 	var pemesan = json.pemesan;
		 	var kendaraan = json.kendaraan;
		
		 	if (pemesan.spk_ppn===1){
		 		$("#spk_ppn").html("YA");
		 	}
		 	if (pemesan.spk_pajak===1){
		 		$("#spk_pajak").html("DIMINTA");
		 	}else{
		 		$("#spk_pajak").html("TIDAK DIMINTA");
		 	}
		 	$("#spk_tgl").html(date_format(pemesan.spk_tgl));
		 	$("#spk_sales").html(pemesan.karyawan_nama + " / " +pemesan.team_nama );

		 	$("#pel_nama").html(pemesan.pel_nama);
		 	$("#pel_alamat").html(pemesan.pel_alamat);
		 	$("#pel_pos").html(pemesan.pel_pos);
		 	$("#pel_telp").html(pemesan.pel_telp);
		 	$("#pel_ponsel").html(pemesan.pel_ponsel);
		 	$("#spk_npwp").html(pemesan.spk_npwp);
		 	$("#spk_fleet").html(pemesan.spk_fleet);

		 	$("#spk_stnk_nama").html(pemesan.pel_nama);
		 	$("#spk_stnk_alamat").html(pemesan.pel_alamat);
		 	$("#spk_stnk_pos").html(pemesan.pel_pos);
		 	$("#spk_stnk_alamatd").html(pemesan.spk_stnk_alamatd);
		 	$("#spk_stnk_posd").html(pemesan.spk_stnk_posd);
		 	$("#spk_stnk_telp").html(pemesan.spk_stnk_telp);
		 	$("#spk_stnk_ponsel").html(pemesan.spk_stnk_ponsel);
		 	$("#spk_stnk_email").html(pemesan.spk_stnk_email);
		 	$("#spk_stnk_ktp").html(pemesan.spk_stnk_ktp);

		 	$("#variant_nama").html(pemesan.type_nama+ " " +pemesan.variant_nama);
		 	$("#spk_warna").html(pemesan.warna_nama);
		 	$("#variant_id").html(pemesan.variant_serial);
		 	if (kendaraan!=null){
				$("#kendaraan .no-data").removeClass("show");
			 	$("#trk_dh").html(kendaraan.trk_dh);
			 	$("#trk_mesin").html(kendaraan.trk_mesin);
			 	$("#trk_rangka").html(kendaraan.trk_rangka);
			 	$("#trk_warna").html(kendaraan.warna_nama);		 		
		 	}else{
				$("#kendaraan .no-data").addClass("show");
			 	$("#trk_dh").html('');
			 	$("#trk_mesin").html('');
			 	$("#trk_rangka").html('');
			 	$("#trk_warna").html('');
		 	}

		 	if(pemesan.spk_kategori==1){		 	
		 		hpp = off+bbn;
		 		$("#spk_kategori").html("ON THE ROAD");
		 		$("#on").show();
		 		$("#bbn").show();
		 		$("#off").attr("readonly","");
		 		$("#on").val(number_format(pemesan.variant_on));
		 	}else{
		 		hpp = off;
		 		bbn=0;
		 		$("#on").hide();
		 		$("#bbn").hide();
		 		$("#off").removeAttr("readonly");
		 		$("#spk_kategori").html("OFF THE ROAD");
		 	}
		 	$("#bbn").val(number_format(bbn));
		 	$("#off").val(number_format(off));
			
		});
	};

	function getDiskon(id) {
		$.ajax({
			type: "GET",
			url: "{{url('api/diskon/request')}}" + "/" + id
		}).done(function(data) {
			$("#cashback").val("");
			$("#keterangan").val("");
			$("#cashback").val(data[0].spk_cashback);
			$("#keterangan").val(data[0].spk_ket);
		});
	}

	function getType(id){
		$.ajax({
			type: "GET",
			url: "{{url('api/kendaraan/type')}}" + "/" + id
		}).done(function(data) {
			$("#type").val(data[0].type_id);
			setVariant(data[0].type_id);
		});
	}

	function setVariant(id){
		$.ajax({
			type: "GET",
			url: "{{url('api/kendaraan/variant')}}" + "/" + id
		}).done(function(data) {	
			$("#spk_kendaraan").html("");			
			jQuery.each(data, function(i, item) {
				$("#spk_kendaraan").append("<option value='"+ item.variant_id +"'>"+ item.variant_type +" - "+ item.variant_nama +"</option>");
			});
		});
	}

function loadData() {

	var db = {
        loadData: function(filter) {
			return $.ajax({
                type: "GET",
                url: "{{url('api/spk/all')}}",
                data: filter
            });
        },

        deleteItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "DELETE",
                url: "{{url('/api/spk')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });
        }
    };

    db.status = [
    		{
            	"status_id": "",
            	"status_nama": "",           
        	},
        	{
            	"status_id": "99",
            	"status_nama": "cancel",           
        	},
    		{
            	"status_id": "9",
            	"status_nama": "INVALID",           
        	},
    		{
            	"status_id": "1",
            	"status_nama": "VALID",           
        	},
    		{
            	"status_id": "2",
            	"status_nama": "MATCHED",           
        	},
    		{
            	"status_id": "3",
            	"status_nama": "DO",           
        	},
        ];

    $("#allSPK").jsGrid({
        height: "100%",
        width: "100%",
 
        sorting: true,
        filtering: true,
        autoload: true,
        paging: true,
        pageSize: 30,
        pageButtonCount: 5,
        noDataContent: "Tidak Ada Data",
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db,
 
        fields: [
            { name: "spk_tgl", title:"Tanggal", type: "text", width: 80, align:"center" },
            { name: "spk_id", title:"No SPK", type: "text", width: 80, align:"center" },
            { name: "spk_pel_nama", title:"Nama Pelanggan", type: "text", width: 170},
           // { name: "spk_type", title:"Type", type: "text", width: 70, align:"center" },
            { name: "spk_variant", title:"Varian", type: "text", width: 150 },
            { name: "spk_warna", title:"Warna", type: "text", width: 70, align:"center"},
            { name: "spk_sales", title:"Sales", type: "text", width: 100},
            { name: "spk_team", title:"Team", type: "text", width: 100, align:"center" },
            { name: "spk_pembayaran", title:"Pembayaran", type: "text", width: 100, align:"right" },
            { name: "spk_via", title:"Via", type: "text", width: 70, align:"center" },
            { name: "spk_pel_kota", title:"Kota", type: "text", width: 70, align:"center" },
            { name: "spk_status", title:"Status", type: "select", width: 70, items: db.status, valueField: "status_id", textField: "status_nama", align:"center" },
            { name: "spk_ket", title:"Keterangan", type: "text", width: 180 },
            { name: "detail" , title:"", width:50, align:"center" },
            { type: "control",  width:70, align:"center", editButton:false, itemTemplate: function(value, item) {
                    var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                    
                    var $customButton = $('<input class="jsgrid-button jsgrid-edit-button" type="button" title="Edit" style="float:left">')
                        .click(function(e) {
                            $("#state").val("edit");

                            $("#form_title").html("EDIT: "+ item.spk_id);
                            $("#btn_save").html("Simpan Perubahan");

                            $("#spk_id").val(item.spk_id);
                            $("#spk_tgl").val(item.spk_tgl);
                            $("#spk_pel_nama").val(item.spk_pel_nama);
                            $("#spk_pel_identitas").val(item.spk_pel_identitas);
                            $("#spk_pel_alamat").val(item.spk_pel_alamat);
                            // $("#spk_pel_kota").val(item.spk_kota);
                            $("#spk_pel_pos").val(item.spk_pel_pos);
                            $("#spk_pel_telp").val(item.spk_pel_telp);
                            $("#spk_pel_ponsel").val(item.spk_pel_ponsel);
                            $("#spk_pel_email").val(item.spk_pel_email);
                            $("#spk_pel_kota").val(item.spk_pel_kota);
                            // $("#spk_warna").val(item.spk_warna);
                            // $("#spk_fleet").val(item.spk_fleet);
                            $("#spk_stnk_nama").val(item.spk_stnk_nama);
                            $("#spk_stnk_ktp").val(item.spk_stnk_ktp);
                            $("#spk_stnk_alamat").val(item.spk_stnk_alamat);
                            $("#spk_stnk_pos").val(item.spk_stnk_pos);
                            $("#spk_stnk_alamatd").val(item.spk_stnk_alamatd);
                            $("#spk_stnk_posd").val(item.spk_stnk_posd);
                            $("#spk_stnk_telp").val(item.spk_stnk_telp);
                            $("#spk_stnk_ponsel").val(item.spk_stnk_ponsel);
                            $("#spk_stnk_email").val(item.spk_stnk_email);

                            $("input[name=kategori]").filter("[value=" + item.spk_pel_kategori + "]").prop('checked', true);

                            $("#spk_sales").val(item.spk_sales_id);
                            $("#spk_pembayaran").val(item.spk_pembayaran_raw);
                            $("#spk_fleet").val(item.spk_fleet);
                            
                            getDiskon(item.spk_id);
                            getType(item.spk_type);
                            $("#spk_kendaraan").val(item.spk_kendaraan);
                            console.log($customButton);

                            $(".modal").modal("open");
                            e.stopPropagation();
                        });
                    
                    return $result.add($customButton);
                } 
            }
        ]
    });
}
loadData();

</script>

@endsection