@extends('layout')

@section('content')

	<div class="content-header">
		<h6>
			<small>Penjualan</small>
			Monitoring SPK
		</h6>
		<ul class="header-tools right">
            <li><a href="{{url('spk')}}" class="chip">Monitoring SPK</a></li>
			<li><a href="{{url('spk/summary')}}" class="chip">Summary SPK</a></li>
			<li><a href="{{url('spk/tracking')}}" class="chip">SPK Tracking</a></li>
			<li><a href="{{url('spk/composition')}}" class="chip">Sales Composition</a></li>
			<li><a href="{{url('spk/cancel')}}" class="chip active">SPK Cancel</a></li>
			<li><a href="{{url('spk/reqfaktur')}}" class="chip">Request Cetak</a></li>
			<li><a href="{{url('spk/allspk')}}" class="chip">All SPK</a></li>
		</ul>
	</div>

<div class="wrapper">
	<div id="dataSPK">

	</div>	
</div>

@include("modules.spk.detail_cancel")

<script>

	function detail(e){
		var id = $(e).attr("data-id");
		$("#detail a[href='#pemesan").trigger("click");
		$("#spk_id").html(id);
		$("#msg").html('');

		$.ajax({
            type: "GET",
            url: "{{url('api/spk/cancel')}}/"+id
        }).done(function(json) {
			var total_aksesoris = 0;
		 	var pemesan = json.pemesan;
		 	var kendaraan = json.kendaraan;
		 	var diskon = json.diskon;
		 	var aksesoris = json.aksesoris;
		 	var leasing = json.leasing;

		 	if (pemesan.spk_ppn===1){
		 		$("#spk_ppn").html("YA");
		 	}
		 	if (pemesan.spk_pajak===1){
		 		$("#spk_pajak").html("DIMINTA");
		 	}else{
		 		$("#spk_pajak").html("TIDAK DIMINTA");
		 	}
		 	$("#spk_tgl").html(date_format(pemesan.spk_tgl));
		 	$("#spk_sales").html(pemesan.karyawan_nama + " / " +pemesan.team_nama );

		 	$("#pel_nama").html(pemesan.spk_pel_nama);
		 	$("#pel_identitas").html(pemesan.spk_pel_identitas);
		 	$("#pel_alamat").html(pemesan.spk_pel_alamat);
		 	$("#pel_pos").html(pemesan.spk_pel_pos);
		 	$("#pel_telp").html(pemesan.spk_pel_telp);
		 	$("#pel_ponsel").html(pemesan.spk_pel_ponsel);
		 	$("#pel_email").html(pemesan.spk_pel_email);
		 	$("#pel_kategori").html(pemesan.spk_pel_kategori);
		 	$("#spk_npwp").html(pemesan.spk_npwp);
		 	$("#spk_fleet").html(pemesan.spk_fleet);

		 	$("#spk_stnk_nama").html(pemesan.spk_stnk_nama);
		 	$("#spk_stnk_alamat").html(pemesan.spk_stnk_alamat);
		 	$("#spk_stnk_pos").html(pemesan.spk_stnk_pos);
		 	$("#spk_stnk_alamatd").html(pemesan.spk_stnk_alamatd);
		 	$("#spk_stnk_posd").html(pemesan.spk_stnk_posd);
		 	$("#spk_stnk_telp").html(pemesan.spk_stnk_telp);	
		 	$("#spk_stnk_ponsel").html(pemesan.spk_stnk_ponsel);
		 	$("#spk_stnk_email").html(pemesan.spk_stnk_email);
		 	$("#spk_stnk_ktp").html(pemesan.spk_stnk_ktp);

		 	$("#variant_nama").html(pemesan.type_nama+ " " +pemesan.variant_nama);
		 	$("#spk_warna").html(pemesan.warna_nama);
		 	$("#variant_id").html(pemesan.variant_serial);
		 	if (kendaraan!=null){
			 	$("#trk_dh").html(kendaraan.trk_dh);
			 	$("#trk_mesin").html(kendaraan.trk_mesin);
			 	$("#trk_rangka").html(kendaraan.trk_rangka);
			 	$("#trk_warna").html(kendaraan.warna_nama);		 		
		 	}else{
			 	$("#trk_dh").html('');
			 	$("#trk_mesin").html('');
			 	$("#trk_rangka").html('');
			 	$("#trk_warna").html('');
		 	}

		 	if(pemesan.spk_kategori==1){		 	
		 		hpp = off+bbn;
		 		$("#spk_kategori").html("ON THE ROAD");
		 	}else{
		 		$("#spk_kategori").html("OFF THE ROAD");
		 	}

		 	if (diskon!=null){
			 	$("#diskon_cashback").html(number_format(diskon.spkd_cashback));
			 	$("#diskon_komisi").html(number_format(diskon.spkd_komisi));
			 	$("#komisi").val(number_format(diskon.spkd_komisi));
			 	$('#aksesoris').html('');
			 	for(var i in aksesoris){
			 		var no = parseInt(i)+1;
			 		var obj = aksesoris[i];
			 		total_aksesoris += obj.spka_harga;
			 		$('#aksesoris').append('<tr class="jsgrid-row"><td class="jsgrid-cell" width="180px" style="padding-left:30px!important">'+ (no) +'. '+ obj.spka_kode +' - '+ obj. spka_nama+'</td><td class="bold jsgrid-cell text-right">'+number_format(obj.spka_harga)+'</td></tr>');

			 	}
			 	total_diskon = diskon.spkd_cashback +  total_aksesoris;
			 	$("#diskon_ket").html(diskon.spkd_ket);
			 	$("#diskon_total").html(number_format(total_diskon));

			}else{
				$("#diskon_cashback").html('');
			 	$("#diskon_komisi").html('');
			 	$("#komisi").val('');
			 	$('#aksesoris').html('');
			 	$("#diskon_ket").html('');
			 	$("#diskon_total").html('');

			}

		 	if (pemesan.spk_pembayaran==0){
		 		$('#spk_metode').html("CASH");
		 	}else{
		 		$('#spk_metode').html("CREDIT"); 		
		 	}

		 	$('#spk_waktu').val(pemesan.spk_waktu);
		 	$('#spk_leasing').val(pemesan.leasing_nama);
		});
	};
	

function loadData() {


	var db_spk = {
        loadData: function(filter) {
			return $.ajax({
                type: "GET",
                url: "{{url('api/spk/cancel')}}",
                data: filter
            });
        },
        onDataLoaded: function(args) {

        }
    };

    $("#dataSPK").jsGrid({
        height: "100%",
        width: "100%",
 
        sorting: true,
        filtering: true,
        autoload: true,
        paging: true,
        pageSize: 30,
        pageButtonCount: 5,
        noDataContent: "Tidak Ada Data",
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db_spk,
 
        fields: [
            { name: "spk_tgl", title:"Tanggal", type: "text", width: 80, align:"center" },
            { name: "spk_id", title:"No SPK", type: "text", width: 80, align:"center" },
            { name: "spk_pel_nama", title:"Nama Pelanggan", type: "text", width: 170},
           // { name: "spk_type", title:"Type", type: "text", width: 70, align:"center" },
            { name: "spk_variant", title:"Varian", type: "text", width: 150 },
            { name: "spk_warna", title:"Warna", type: "text", width: 70, align:"center"},

            { name: "spk_sales", title:"Sales", type: "text", width: 100},

            { name: "spk_team", title:"Team", type: "text", width: 100, align:"center" },

            { name: "spk_pembayaran", title:"Pembayaran", type: "text", width: 100, align:"right" },
            { name: "spk_via", title:"Via", type: "text", width: 70, align:"center" },
            { name: "spk_kota", title:"Kota", type: "text", width: 70, align:"center" },
            { name: "spk_ket", title:"Keterangan", type: "text", width: 180 },
            { name: "detail", title:"", width:50, align:"center" }
        ]
    });
}
loadData();

</script>

@endsection