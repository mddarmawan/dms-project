
<div id="detail" class="modal" style="width:960px;overflow-y:hidden">   
    <h6 class="modal-title blue-grey darken-1">
        <span id="spk_id">SPK</span>
        <i class="fa fa-calendar" style="margin:0 5px 0 25px"></i> <span id="spk_tgl"></span>
        <i class="fa fa-user"  style="margin:0 5px 0 25px"></i> <span id="spk_sales"></span>
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <ul class="tabs blue-grey darken-1 white-text">
        <li class="tab col s3"><a class="active" href="#pemesan" >Pemesan</a></li>
        <li class="tab col s3"><a href="#kendaraan">Kendaraan</a></li>
        <li class="tab col s3"><a href="#diskon">Diskon</a></li>
      </ul>
    <div class="modal-content" style="padding:0px;position: relative;">
      	<div id="pemesan">
      		<div class="row" style="margin:0">
      			<div class="col s12 m6">
      				<table class="info">
      					<tr>
      						<td width="180px">Nama Pemesan</td>
      						<td width="10px">:</td>
      						<td class="bold" id="pel_nama"></td>
      					</tr>
      					<tr>
      						<td width="180px">Alamat Domisili/Usaha</td>
      						<td width="10px">:</td>
      						<td class="bold" id="pel_alamat"></td>
      					</tr>
      					<tr>
      						<td width="180px">Kode Pos</td>
      						<td width="10px">:</td>
      						<td class="bold" id="pel_pos"></td>
      					</tr>
      					<tr>
      						<td width="180px">Telepon</td>
      						<td width="10px">:</td>
      						<td class="bold" id="pel_telp"></td>
      					</tr>
      					<tr>
      						<td width="180px">Ponsel</td>
      						<td width="10px">:</td>
      						<td class="bold" id="pel_ponsel"></td>
      					</tr>
      					<tr>
      						<td width="180px">Email</td>
      						<td width="10px">:</td>
      						<td class="bold" id="pel_email"></td>
      					</tr>
      					<tr>
      						<td width="180px">Kode PPN</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_ppn"></td>
      					</tr>
      					<tr>
      						<td width="180px">NPWP</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_npwp"></td>
      					</tr>
      					<tr>
      						<td width="180px">Faktur Pajak</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_pajak"></td>
      					</tr>
      					<tr>
      						<td colspan="3">Nama dan Jabatan Contact Person Customer Corporate/Fleet:</td>
      					</tr>
      					<tr>
      						<td colspan="3" id="spk_fleet"></td>
      					</tr>
      				</table>
      			</div>      			
      			<div class="col s12 m6"  style="border-left:1px solid #ddd">
      				<table class="info">
      					<tr>
      						<td width="180px">Faktur STNK a/n</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_stnk_nama"></td>
      					</tr>
      					<tr>
      						<td width="180px">No. KTP</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_stnk_ktp"></td>
      					</tr>
      					<tr>
      						<td width="180px">Alamat KTP/KIMS</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_stnk_alamat"></td>
      					</tr>
      					<tr>
      						<td width="180px">Kode Pos</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_stnk_pos"></td>
      					</tr>
      					<tr>
      						<td width="180px">Alamat Tempat Tinggal</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_stnk_alamatd"></td>
      					</tr>
      					<tr>
      						<td width="180px">Kode Pos</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_stnk_posd"></td>
      					</tr>
      					<tr>
      						<td width="180px">Telepon</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_stnk_telp"></td>
      					</tr>
      					<tr>
      						<td width="180px">Ponsel</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_stnk_ponsel"></td>
      					</tr>
      					<tr>
      						<td width="180px">Email</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_stnk_email"></td>
      					</tr>
      				</table>
      			</div>
      		</div>
      	</div>
      	<div id="kendaraan">
      		<div class="row" style="margin:0">
		      	<div class="col m6"  style="border-right:1px solid #ddd;">
		      		<table class="info">
		      			<tr>
		      				<td width="180px">Variant</td>
		      				<td width="10px">:</td>
		      				<td class="bold" id="variant_nama"></td>
		      			</tr>
		      			<tr>
		      				<td width="180px">ID</td>
		      				<td width="10px">:</td>
		      				<td class="bold" id="variant_id"></td>
		      			</tr>
		      			<tr>
		      				<td width="180px">Warna</td>
		      				<td width="10px">:</td>
		      				<td class="bold" id="spk_warna"></td>
		      			</tr>
		      			<tr>
		      				<td width="180px">Keterangan</td>
		      				<td width="10px">:</td>
		      				<td class="bold" id="spk_kategori"></td>
		      			</tr>
		      		</table>
	      		</div>      		
		      	<div class="col m6">
		      		<table class="info">
	      				<tr>
			      			<td width="140px">Jenis Pembayaran</td>
			      			<td width="10px">:</td>
			      			<td class="bold" id="spk_metode"></td>
			      		</tr>
	      				<tr class="leasing">
			      			<td width="140px">Leasing</td>
			      			<td width="10px">:</td>
			      			<td class="bold" id="spk_leasing"></td>
			      		</tr>
		      		</table>
		      	</div>
	      	</div>
      	</div>
      	<div id="diskon" style="padding:0px">
      		<div class="row" style="margin:0px; padding:0px;">
      			<div class="col m5"  style="border-right:1px solid #ddd">
      				<table class="info payment" style="border:0;margin-bottom:0">
	      					<tr class="jsgrid-row">
	      						<td class="jsgrid-cell" width="180px">Cashback</td>
	      						<td class="jsgrid-cell bold text-right" id="diskon_cashback"></td>
	      					</tr>
	      					<tr class="jsgrid-row">
	      						<td class="jsgrid-cell" colspan="2">Aksesoris</td>
	      					</tr>
	      			</table>
	      			<table id="aksesoris" class="info payment" style="border:0;margin:0">
	      			</table>
	      			<table class="info payment" style="border:0;margin-top:0">
	      					<tr class="jsgrid-row">
	      						<td class="jsgrid-cell" width="180px">Komisi</td>
	      						<td class="bold jsgrid-cell text-right" id="diskon_komisi"></td>
	      					</tr>
	      					<tr class="jsgrid-row">
	      						<td class="jsgrid-cell bold text-right" style="background: #f3f3f3">TOTAL</td>
	      						<td class="jsgrid-cell bold text-right" style="background: #f3f3f3" id="diskon_total"></td>
	      					</tr>
      				</table>
      			</div>
      			<div class="col m7">
      				<table class="info" style="margin-top:0">
      					<tr>
      						<td colspan="3">Keterangan</td>
      					</tr>
      					<tr>
      						<td class="bold" colspan="3" id="diskon_ket"></td>
      					</tr>
      				</table>
      			</div>
      		</div>
      	</div>
    </div>
</div>
