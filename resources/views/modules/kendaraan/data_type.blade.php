<div id="dataType">

</div>

<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script>

    <script>
      // Initialize Firebase
      var config = {
        apiKey: "AIzaSyAbGQT6dUxS5arqAz5DgG1RskqUMX-XOPk",
        authDomain: "dealer-ms.firebaseapp.com",
        databaseURL: "https://dealer-ms.firebaseio.com",
        projectId: "dealer-ms",
        storageBucket: "dealer-ms.appspot.com",
        messagingSenderId: "950788435795"
      };
      firebase.initializeApp(config);
      firebase.auth().signInWithEmailAndPassword("encar.dms.app@gmail.com", ".encar87").catch(function(error) {
          console.log(error.message);
        });
    </script>


<script>
$(function() {
        var db = {
            loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/kendaraan/type')}}",
                    data: filter
                });
            },

            insertItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "POST",
                    url: "{{url('/api/kendaraan/type')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    var db = firebase.database().ref("data/tb_type/" + item.type_id + "/");
                    db.set({
                        type_id: item.type_id,
                        type_nama: item.type_nama,
                        type_status: item.type_status
                    });
                });    
            },

            updateItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "PUT",
                    url: "{{url('/api/kendaraan/type')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    var db = firebase.database().ref("data/tb_type/" + item.type_id + "/");
                    db.set({
                        type_id: item.type_id,
                        type_nama: item.type_nama,
                        type_status: item.type_status
                    });
                });
            },

            deleteItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "DELETE",
                    url: "{{url('/api/kendaraan/type')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(response){
                    firebase.database().ref("data/tb_type/" + item.type_id + "/").remove();                   
                });
            }
        };

        $("#dataType").jsGrid({
            height: "100%",
            width: "100%",
     
            filtering: true,
            editing: true,
            inserting: true,
            sorting: true,
            autoload: true,
     
            deleteConfirm: "Anda yakin akan menghapus data ini?",
     
            controller: db,
     
            fields: [
                { name: "type_nama", title:"Type", type: "text", width: 120, validate: "required" },
                { name: "type_poin", title:"Point", type: "number", width: 80, validate: "required" },
                { type: "control", width:70 }
            ]
        });
});
</script>