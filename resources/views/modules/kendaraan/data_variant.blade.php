<div id="dataVariant">

</div>

<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script>
    <script>
      // Initialize Firebase
      var config = {
        apiKey: "AIzaSyAbGQT6dUxS5arqAz5DgG1RskqUMX-XOPk",
        authDomain: "dealer-ms.firebaseapp.com",
        databaseURL: "https://dealer-ms.firebaseio.com",
        projectId: "dealer-ms",
        storageBucket: "dealer-ms.appspot.com",
        messagingSenderId: "950788435795"
      };
      firebase.initializeApp(config);
      firebase.auth().signInWithEmailAndPassword("encar.dms.app@gmail.com", ".encar87").catch(function(error) {
          console.log(error.message);
        });
    </script>

<script>

function type() {
    $.ajax({
        type: "GET",
        url: "{{url('api/kendaraan/type')}}"
    }).done(function(type) {
        type.unshift({ type_id: "0", type_nama: "" });

function variant() {


           var db = {
            loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/kendaraan/variant')}}",
                    data: filter
                });
            },

           insertItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "POST",
                    url: "{{url('/api/kendaraan/variant')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    var db = firebase.database().ref("data/tb_variant/" + item.variant_id + "/");
                    db.set({
                        variant_id: item.variant_id,
                        variant_nama: item.variant_nama,
                        variant_type: item.variant_type,
                        variant_on: item.variant_on,
                        variant_status: item.variant_status
                    });
                });    
            },

            updateItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "PUT",
                    url: "{{url('/api/kendaraan/variant')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    var db = firebase.database().ref("data/tb_variant/" + item.variant_id + "/");
                    db.set({
                        variant_id: item.variant_id,
                        variant_nama: item.variant_nama,
                        variant_type: item.variant_type,
                        variant_on: item.variant_on,
                        variant_status: item.variant_status
                    });
                });
            },

            deleteItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "DELETE",
                    url: "{{url('/api/kendaraan/variant')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(response){
                    firebase.database().ref("data/tb_variant/" + item.variant_id + "/").remove();                   
                });
            }
        };

        db.status = [{'status_id':'', 'status_nama':''},{'status_id':1, 'status_nama':'ACTIVE'}, {'status_id':0, 'status_nama':'NON ACTIVE'}]

        $("#dataVariant").jsGrid({
            height: "100%",
            width: "100%",
     
            filtering: true,
            editing: true,
            inserting: true,
            sorting: true,
            autoload: true,
     
            deleteConfirm: "Anda yakin akan menghapus data ini?",
     
            controller: db,
     
            fields: [
                { name: "variant_type", title:"Type", type: "select", items: type, valueField: "type_id", textField: "type_nama", width: 100, align:"left" },
                { name: "variant_serial", title:"ID", type: "text", width: 100, validate: "required" },
                { name: "variant_nama", title:"Nama", type: "text", width: 130, validate: "required" },
                { name: "variant_ket", title:"Keterangan", type: "text", width: 170, validate: "required" },
                { name: "variant_status", title:"Status", type: "select", items: db.status, valueField: "status_id", textField: "status_nama", width: 100, align:"left"},
                { name: "type_poin", title:"Point", type: "number", width: 50, inserting: false, editing:false },
                { type: "control", width:70 }
            ]

        });

    
};
variant();

});
};
type();
</script>