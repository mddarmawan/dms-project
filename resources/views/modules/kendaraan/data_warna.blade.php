<div id="dataWarna">

</div>

<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script>

    <script>
      // Initialize Firebase
      var config = {
        apiKey: "AIzaSyAbGQT6dUxS5arqAz5DgG1RskqUMX-XOPk",
        authDomain: "dealer-ms.firebaseapp.com",
        databaseURL: "https://dealer-ms.firebaseio.com",
        projectId: "dealer-ms",
        storageBucket: "dealer-ms.appspot.com",
        messagingSenderId: "950788435795"
      };
      firebase.initializeApp(config);
      firebase.auth().signInWithEmailAndPassword("encar.dms.app@gmail.com", ".encar87").catch(function(error) {
          console.log(error.message);
        });
    </script>


<script>
$(function() {
        var db = {
            loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/warna')}}",
                    data: filter
                });
            },

            insertItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "POST",
                    url: "{{url('/api/warna')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    var db = firebase.database().ref("data/tb_warna/" + item.warna_id + "/");
                    db.set({
                        warna_id: item.warna_id,
                        warna_nama: item.warna_nama
                    });
                });    
            },

            updateItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "PUT",
                    url: "{{url('/api/warna')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    var db = firebase.database().ref("data/tb_warna/" + item.warna_id + "/");
                    db.set({
                        warna_id: item.warna_id,
                        warna_nama: item.warna_nama
                    });
                });
            },

            deleteItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "DELETE",
                    url: "{{url('/api/warna')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(response){
                    firebase.database().ref("data/tb_warna/" + item.warna_id + "/").remove();                   
                });
            }
        };

        $("#dataWarna").jsGrid({
            height: "100%",
            width: "100%",
     
            filtering: true,
            editing: true,
            inserting: true,
            sorting: true,
            autoload: true,
     
            deleteConfirm: "Anda yakin akan menghapus data ini?",
     
            controller: db,
     
            fields: [
                { name: "warna_nama", title:"nama", type: "text", width: 120, validate: "required" },
                { type: "control", width:70 }
            ]
        });
});
</script>