@extends('layout')

@section('content')

	<div class="content-header">
		<h6>
			<small>Persediaan</small>
			Kendaraan
		</h6>
	</div>


<div class="wrapper">
	<div class="row" style="margin:0;height:100%;position:relative;">
        <div class="col s12 m3" style="padding:0;height:100%;position:relative;">
        	@include("modules.kendaraan.data_type")  
        </div>
	    <div class="col s12 m9" style="padding:0;height:100%;position:relative;">
	        @include("modules.kendaraan.data_variant")  
	    </div>
    </div>
</div>


@endsection