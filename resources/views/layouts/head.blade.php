    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="">
    <title>Encar Daihatsu</title>
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="theme-color" content="#EE6E73">

	<link href="/assets/images/launcher.png" rel="icon" type="image/x-icon">
    <link href="http://fonts.googleapis.com/css?family=Inconsolata" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/assets/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/prism.css" rel="stylesheet">
    <link href="/assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="/assets/css/jsgrid.min.css" rel="stylesheet">
    <link href="/assets/css/jsgrid-theme.css" rel="stylesheet">
    <link href="/assets/css/jquery-ui.min.css" rel="stylesheet">
    <link href="/assets/css/businessicon.css" rel="stylesheet">
    <link href="/assets/css/custom.css" rel="stylesheet">


    <script src="/assets/js/jquery-2.1.4.min.js"></script>
	<script src="/assets/js/jquery-ui.min.js"></script>
    <script src="/assets/js/jsgrid.min.js"></script>
    <script src="/assets/js/helper.js"></script>