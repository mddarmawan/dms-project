<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ApiLaporanController extends Controller
{
    function f_kendaraan($id){
		$data = DB::table('tb_spk_faktur')
    		->join('tb_spk', 'tb_spk_faktur.spkf_spk', '=', 'tb_spk.spk_id')
    		->join('tb_spk_diskon', 'tb_spk_diskon.spkd_spk', '=', 'tb_spk.spk_id')
    		->join('tb_tr_kendaraan', 'tb_tr_kendaraan.trk_dh', '=', 'tb_spk.spk_dh')
    		->join('tb_spk_leasing', 'tb_spk_leasing.spkl_spk', '=', 'tb_spk.spk_id')
    		->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
    		->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
    		->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
    		->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
    		->join('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
    		->leftjoin('tb_referral', 'tb_spk_diskon.spkd_ref_nama', '=', 'tb_referral.referral_nama')
    		->leftjoin('tb_asuransi', 'tb_spk_leasing.spkl_asuransi', '=', 'tb_asuransi.asuransi_id')
    		->leftjoin('tb_asuransi_jenis', 'tb_spk_leasing.spkl_jenis_asuransi', '=', 'tb_asuransi_jenis.ajenis_id')
    		->leftjoin('tb_leasing', 'tb_leasing.leasing_id', '=', 'tb_spk.spk_leasing')
    		->leftjoin('tb_warna', 'warna_id', '=', 'tb_spk.spk_warna')
    		->where('spkf_id', $id)
			->get();
		$result = $data;

		$data = array();
		foreach($result as $r){
			$item = array();
			$item['spkf_id'] = $r->spkf_id;
			$item['spkf_tgl'] = date_format(date_create($r->spkf_tgl),"d/m/Y");
			$item['spk_id'] = $r->spk_id;
			$item['spk_referral'] = strtoupper($r->referral_nama);
			$item['spk_tgl'] = date_format(date_create($r->spk_tgl),"d/m/Y");
            $item['spk_pel_nama'] = strtoupper($r->spk_pel_nama);
            $item['spk_pel_alamat'] = strtoupper($r->spk_pel_alamat); 
            $item['spk_stnk_nama'] = strtoupper($r->spk_stnk_nama);
            $item['spk_stnk_alamat'] = strtoupper($r->spk_stnk_alamat);
            $item['spk_dpp'] = ucwords($r->spk_dpp);
            $item['spk_bbn'] = ucwords($r->spk_bbn);
			$item['spk_type'] = ucwords($r->type_nama);
			$item['spk_variant'] = ucwords($r->type_nama." ".$r->variant_nama);
			$item['spk_warna'] = ucwords ($r->warna_nama);
			$item['spk_no_rangka'] = strtoupper($r->trk_rangka);
			$item['spk_no_mesin'] = strtoupper($r->trk_mesin);
			$item['spk_serial'] = strtoupper($r->variant_serial);
			$item['spk_sales'] = ucwords($r->karyawan_nama);
			$item['spk_team'] = strtoupper($r->team_nama);
			$item['spk_pembayaran'] = number_format(DB::table('tb_spk_pembayaran')->select(DB::raw("SUM(spkp_jumlah) as bayar"))
    		->where("spkp_spk",$r->spk_id)->first()->bayar,0,",",".");
    		$item['spk_riwayat_pembayaran'] = DB::table('tb_spk_pembayaran')->where('spkp_spk', $r->spk_id)->get();
			$item['spk_pel_kota'] = strtoupper($r->spk_pel_kota);
			$item['spk_metode'] = ($r->spk_pembayaran == 0 ? 'CASH' : 'CREDIT');
			if ($r->spk_pembayaran == 0){
				$item['spk_via'] = "CASH";		
			}else{
				$item['spk_via'] = $r->leasing_nick;			
			}
			$item['spk_status'] = $r->spk_status;
			$item['spk_ket'] = "-";
			$item['spk_cashback'] = $r->spkd_cashback;
			$item['spk_asuransi'] = ucwords($r->asuransi_nama);
			$item['spk_asuransi_jenis'] = ucwords($r->ajenis_nama);
			$item['spk_waktu'] = ucwords($r->spkl_waktu);
			$item['spk_leasing'] = ucwords($r->leasing_nama);
			$item['spk_dh'] = $r->trk_dh;
			$item['spk_variant_off'] = number_format(($r->variant_on - $r->variant_bbn), 0, ",", ".");
			$item['spk_variant_on'] = number_format($r->variant_on, 0, ",", ".");
			$item['spk_variant_bbn'] = number_format($r->variant_bbn, 0, ",", ".");

			$item['diskon'] = DB::table('tb_spk_diskon')
	    		->where('spkd_status',1)
	    		->where('spkd_spk', $r->spk_id)->get();

	    	$item['aksesoris'] = array();
	    	if ($item['diskon']){
		    	$item['aksesoris'] = DB::table('tb_spk_aksesoris')
		    		->where('spka_diskon', $item['diskon'][0]->spkd_id)->get();
	    	}

			array_push($data, $item);
		}

    	return json_encode($data);
    }

    function f_BAST($id){
		$data = DB::table('tb_spk_faktur')
    		->join('tb_spk', 'tb_spk_faktur.spkf_spk', '=', 'tb_spk.spk_id')
    		->join('tb_spk_diskon', 'tb_spk_diskon.spkd_spk', '=', 'tb_spk.spk_id')
    		->join('tb_tr_kendaraan', 'tb_tr_kendaraan.trk_dh', '=', 'tb_spk.spk_dh')
    		->join('tb_spk_leasing', 'tb_spk_leasing.spkl_spk', '=', 'tb_spk.spk_id')
    		->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
    		->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
    		->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
    		->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
    		->join('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
    		->leftjoin('tb_warna', 'warna_id', '=', 'tb_spk.spk_warna')
    		->where('spkf_id', $id)
			->get();
		$result = $data;

		$data = array();
		foreach($result as $r){
			$item = array();
			$item['spkf_id'] = $r->spkf_id;
			$item['spkf_tgl'] = date_format(date_create($r->spkf_tgl),"d/m/Y");
			$item['spk_id'] = $r->spk_id;
			$item['spk_referral'] = strtoupper($r->referral_nama);
			$item['spk_tgl'] = date_format(date_create($r->spk_tgl),"d/m/Y");
            $item['spk_pel_nama'] = strtoupper($r->spk_pel_nama);
            $item['spk_pel_alamat'] = strtoupper($r->spk_pel_alamat); 
            $item['spk_stnk_nama'] = strtoupper($r->spk_stnk_nama);
            $item['spk_stnk_alamat'] = strtoupper($r->spk_stnk_alamat);
			$item['spk_type'] = ucwords($r->type_nama);
			$item['spk_variant'] = ucwords($r->type_nama." ".$r->variant_nama);
			$item['spk_warna'] = ucwords ($r->warna_nama);
			$item['spk_no_rangka'] = strtoupper($r->trk_rangka);
			$item['spk_no_mesin'] = strtoupper($r->trk_mesin);
			$item['spk_serial'] = strtoupper($r->variant_serial);
			$item['spk_sales'] = ucwords($r->karyawan_nama);
			$item['spk_team'] = strtoupper($r->team_nama);
			$item['spk_pembayaran'] = number_format(DB::table('tb_spk_pembayaran')->select(DB::raw("SUM(spkp_jumlah) as bayar"))
    		->where("spkp_spk",$r->spk_id)->first()->bayar,0,",",".");
    		
			$item['spk_pel_kota'] = strtoupper($r->spk_pel_kota);
			$item['spk_metode'] = ($r->spk_pembayaran == 0 ? 'CASH' : 'CREDIT');
			if ($r->spk_pembayaran == 0){
				$item['spk_via'] = "CASH";		
			}else{
				$item['spk_via'] = $r->leasing_nick;			
			}
			$item['spk_status'] = $r->spk_status;
			$item['spk_ket'] = "-";
		
			
			array_push($data, $item);
		}

    	return json_encode($data);
    }


}
