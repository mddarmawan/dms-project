<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ApiNospkController extends Controller
{
    function index(){
    	$data = DB::table('tb_spk_no')
                ->join('tb_sales', 'tb_spk_no.spkNo_sales', '=', 'tb_sales.sales_id')
                ->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
                ->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
                ->get();
		$result = $data->filter(function ($data) {
		    return 
		    	(!request("spkNo_min	") || strrpos(strtolower($data->spkNo_min	), strtolower(request("spkNo_min	"))) > -1) &&
				 (!request("spkNo_max") || strrpos(strtolower($data->spkNo_max), strtolower(request("spkNo_max"))) > -1) &&
				 (!request("spkNo_sales") || strrpos(strtolower($data->karyawan_nama), strtolower(request("spkNo_sales"))) > -1) &&
                 (!request("spkNo_team") || strrpos(strtolower($data->team_nama), strtolower(request("spkNo_sales"))) > -1);
		});

        $data = array();
        foreach($result as $r){
            $item = array();
            $item['spkNo_id'] = $r->spkNo_id;
            $item['spk_id'] = $r->spk_id;
            $item['spkNo_sales'] = $r->karyawan_nama;
            $item['spkNo_team'] = $r->team_nama;
            array_push($data, $item);
        }

        return json_encode($data);
    }

    function lastNo(){
        $data = DB::table('tb_spk_no')
                ->join('tb_sales', 'tb_spk_no.spkNo_sales', '=', 'tb_sales.sales_id')
                ->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
                ->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
                ->orderBy('spkNo_id', 'desc')
                ->first();

        return json_encode($data);
    }

    function store(){
    	$this->validate(request(), [
            "spkNo_sales"     => "required"
        ]);

        $insert = array();
        $spk = array();
        for ($id = request('spkNo_min'); $id <= request('spkNo_max'); $id++)
        {
            $spk_id = "17-" . sprintf("%05s", $id); 
            $item = array(
                "spk_id"     =>  $spk_id,
                "spkNo_sales"   =>  request("spkNo_sales")
            );
            $insert[] = $item;
            $spk[] = $spk_id;
        }

        DB::table('tb_spk_no')->insert($insert);

        $data['spk_id'] = $spk;
        $data['spkNo_sales'] = request("spkNo_sales");
        $data['status'] = 1;

        $result[request("spkNo_sales")] = str_replace('"',"'", json_encode($spk));

        return json_encode($result);
    }

    function update(){
    	$this->validate(request(), [
            "spkNo_min"      => "required",
            "spkNo_max"      => "required",
            "spkNo_sales"     => "required"
        ]);

	    DB::table('tb_warna')-> where("spkNo_id",request("spkNo_id"))->update([
	       "spkNo_min"     =>  request("spkNo_min"),
            "spkNo_max"     =>  request("spkNo_max"),
            "spkNo_sales"   =>  request("spkNo_sales")
            ]);

	    return json_encode(DB::table('tb_spk_no')-> where("spkNo_id",request("spkNo_id"))->first());
    }

     function destroy(){
        return DB::table('tb_spk_no')->where('spkNo_id', request("spkNo_id"))->delete();
    }

    /**
     * Auto Generate ID
     *
     * @return String
     */
    public function autoId($suffix, $min, $max)
    {
        for ($id = $min; $id <= $max; $id++)
        {
            $id = $suffix . sprintf("%04s", $id);
            return $id;
        }
    }

}