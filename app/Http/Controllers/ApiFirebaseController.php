<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ApiFirebaseController extends Controller
{
    function sales_input(){
        $validator = Validator::make(request()->all(), [
            "sales_Uid"        => "required",
            "sales_id"        => "required",
        ]);

        $proses['result'] = false;
        $proses['msg'] = "";

        if ($validator->fails()) {
            $proses['msg'] =  $validator->messages();

        } else {
            if (DB::table('tb_sales')-> where("sales_Uid",request("sales_Uid"))->first()) {
                $proses['result'] = DB::table('tb_sales')-> where("sales_Uid",request("sales_Uid"))->update([
                    "sales_id"    =>  request("akun_id"),
                    "sales_usr"    =>  request("akun_nama"),
                ]);

                $proses['result'] = 1;  
            } else {
                $proses['result'] = DB::table('tb_sales')->insert([
                    "sales_Uid"     =>  request("sales_Uid"),
                    "sales_id"    =>  request("akun_id"),
                    "sales_usr"    =>  request("akun_nama"),
                ]); 

                $proses['result'] = 1;       
            }

        }
        
        return json_encode($proses);
    }


     function salesUid_input(){
        $this->validate(request(), [
            "salesUid_id"        => "required",
            "salesUid_spk"        => "required",
            "salesUid_spkd"        => "required",
        ]);

        
        $insert = array(
            "salesUid_id"     =>  request("salesUid_id"),
            "salesUid_spk"     =>  request("salesUid_spk"),
            "salesUid_spkd"   =>  request("salesUid_spkd")
            );

        $id = DB::table('tb_sales_Uid')->insertGetId($insert, 'salesUid_id');
        return json_encode(DB::table('tb_sales_Uid')->where("salesUid_id", $id)->first());
    }

    function add_spk(){

        $proses = 0;

        $result = DB::table('tb_pelanggan')->where("pel_identitas", request("spk_pel_identitas"))->get();
        if (count($result) == 0){
            $insert = DB::table('tb_pelanggan')->insert([
                "pel_nama"      => request("spk_pel_nama"),
                "pel_identitas" => request("spk_pel_identitas"),
                "pel_lahir"     => date_format(date_create(str_replace("/","-", request("spk_pel_lahir"))),"Y-m-d"),
                "pel_alamat"    => request("spk_pel_alamat"),
                "pel_kota"      => request("spk_pel_kota"),
                "pel_pos"       => request("spk_pel_pos"),
                "pel_telp"      => request("spk_pel_telp"),
                "pel_ponsel"    => request("spk_pel_ponsel"),
                "pel_email"     => request("spk_pel_email"),
                "pel_sales"     => request("spk_sales")
            ]);
        } else {
            $insert = DB::table('tb_pelanggan')->where("pel_identitas", request("spk_pel_identitas"))->update([
                "pel_nama"      => request("spk_pel_nama"),
                "pel_identitas" => request("spk_pel_identitas"),
                "pel_lahir"     => date_format(date_create(str_replace("/","-", request("spk_pel_lahir"))),"Y-m-d"),
                "pel_alamat"    => request("spk_pel_alamat"),
                "pel_kota"      => request("spk_pel_kota"),
                "pel_pos"       => request("spk_pel_pos"),
                "pel_telp"      => request("spk_pel_telp"),
                "pel_ponsel"    => request("spk_pel_ponsel"),
                "pel_email"     => request("spk_pel_email"),
                "pel_sales"     => request("spk_sales")
            ]);
        }

        $pelanggan = DB::table('tb_pelanggan')-> where("pel_identitas", request("spk_pel_identitas"))->first();

        $result =  DB::table('tb_spk')->where("spk_id", request("spk_id"))->get();
        if (count($result) == 0){
            $insert = DB::table('tb_spk')->insert([
                "spk_id"            => request("spk_id"),	
                "spk_tgl"           => date_format(date_create(str_replace("/","-",request("spk_tgl"))),"Y-m-d"),
                
                "spk_lat"            => request("spk_lat"),
                "spk_lng"            => request("spk_lng"),
                "spk_pajak"       => request("spk_pajak"),
                "spk_npwp"       => request("spk_npwp"),
                "spk_fleet"         => request("spk_fleet"),
                "spk_pel_nama"      => request("spk_pel_nama"),
                "spk_pel_tgl"           => date_format(date_create(str_replace("/","-",request("spk_pel_tgl"))),"Y-m-d"),
                "spk_pel_identitas" => request("spk_pel_identitas"),
                "spk_pel_alamat"    => request("spk_pel_alamat"),
                "spk_pel_provinsi"      => request("spk_pel_provinsi"),
                "spk_pel_kota"      => request("spk_pel_kota"),
                "spk_pel_pos"       => request("spk_pel_pos"),
                "spk_pel_telp"      => request("spk_pel_telp"),
                "spk_pel_ponsel"    => request("spk_pel_ponsel"),
                "spk_pel_email"     => request("spk_pel_email"),
                "spk_pel_kategori"  => request("spk_pel_kategori"),
                "spk_stnk_nama"     => request("spk_stnk_nama"),
                "spk_stnk_identitas"     => request("spk_stnk_identitas"),
                "spk_stnk_alamat"   => request("spk_stnk_alamat"),
                "spk_stnk_provinsi"     => request("spk_stnk_provinsi"),
                "spk_stnk_kota"     => request("spk_stnk_kota"),
                "spk_stnk_pos"      => request("spk_stnk_pos"),
                "spk_stnk_alamatd"  => request("spk_stnk_alamatd"),
                "spk_stnk_posd"     => request("spk_stnk_posd"),
                "spk_stnk_provinsid"     => request("spk_stnk_provinsid"),
                "spk_stnk_kotad"     => request("spk_stnk_kotad"),
                "spk_stnk_telp"     => request("spk_stnk_telp"),
                "spk_stnk_ponsel"   => request("spk_stnk_ponsel"),
                "spk_stnk_email"    => request("spk_stnk_email"),
                "spk_pembayaran"    => request("spk_pembayaran"),
                "spk_leasing"       => request("spk_leasing"),
                "spk_sales"         => request("spk_sales"),
                "spk_kendaraan"         => request("spk_kendaraan"),
                "spk_kendaraan_harga"         => request("spk_kendaraan_harga"),
                "spk_warna"         => request("spk_warna"),
                "spk_dp"         => request("spk_dp"),
                "spk_ket_permintaan"         => request("spk_ket_permintaan")

            ]);
        } else {
            $insert = DB::table('tb_spk')-> where('spk_id', request('spk_id'))->update([
                "spk_id"            => request("spk_id"),	
                "spk_tgl"           => date_format(date_create(str_replace("/","-",request("spk_tgl"))),"Y-m-d"),
                
                "spk_lat"            => request("spk_lat"),
                "spk_lng"            => request("spk_lng"),
                "spk_pajak"       => request("spk_pajak"),
                "spk_npwp"       => request("spk_npwp"),
                "spk_fleet"         => request("spk_fleet"),
                "spk_pel_nama"      => request("spk_pel_nama"),
                "spk_pel_tgl"           => date_format(date_create(str_replace("/","-",request("spk_pel_tgl"))),"Y-m-d"),
                "spk_pel_identitas" => request("spk_pel_identitas"),
                "spk_pel_alamat"    => request("spk_pel_alamat"),
                "spk_pel_provinsi"      => request("spk_pel_provinsi"),
                "spk_pel_kota"      => request("spk_pel_kota"),
                "spk_pel_pos"       => request("spk_pel_pos"),
                "spk_pel_telp"      => request("spk_pel_telp"),
                "spk_pel_ponsel"    => request("spk_pel_ponsel"),
                "spk_pel_email"     => request("spk_pel_email"),
                "spk_pel_kategori"  => request("spk_pel_kategori"),
                "spk_stnk_nama"     => request("spk_stnk_nama"),
                "spk_stnk_identitas"     => request("spk_stnk_identitas"),
                "spk_stnk_alamat"   => request("spk_stnk_alamat"),
                "spk_stnk_provinsi"     => request("spk_stnk_provinsi"),
                "spk_stnk_kota"     => request("spk_stnk_kota"),
                "spk_stnk_pos"      => request("spk_stnk_pos"),
                "spk_stnk_alamatd"  => request("spk_stnk_alamatd"),
                "spk_stnk_posd"     => request("spk_stnk_posd"),
                "spk_stnk_provinsid"     => request("spk_stnk_provinsid"),
                "spk_stnk_kotad"     => request("spk_stnk_kotad"),
                "spk_stnk_telp"     => request("spk_stnk_telp"),
                "spk_stnk_ponsel"   => request("spk_stnk_ponsel"),
                "spk_stnk_email"    => request("spk_stnk_email"),
                "spk_pembayaran"    => request("spk_pembayaran"),
                "spk_leasing"       => request("spk_leasing"),
                "spk_sales"         => request("spk_sales"),
                "spk_kendaraan"         => request("spk_kendaraan"),
                "spk_kendaraan_harga"         => request("spk_kendaraan_harga"),
                "spk_warna"         => request("spk_warna"),
                "spk_dp"         => request("spk_dp"),
                "spk_ket_permintaan"         => request("spk_ket_permintaan"),
                "spk_pel_fotoid"         => request("spk_pel_fotoid"),
                "spk_stnk_fotoid"         => request("spk_pel_fotoid"),
            ]);
        }

        $result =  SPK::where("spk_id", request('spk_id'))->get();
        if (count($result) > 0){
            $proses = 1;
        } else {
            $proses = 0;
            return $insert;
        }

        return $proses;
    }

    function add_diskon(){

        $proses = 0;

        $result =  DB::table('tb_spk_diskon')-> where("spkd_spk", request("spk_id"))->get();
        if (count($result) == 0){
            $insert =DB::table('tb_spk_diskon')->insert([
                "spkd_spk"          => request("spk_id"),
                "spkd_cashback"     => str_replace(".", "", request("spkd_cashback")),
                "spkd_komisi"       => request("spkd_komisi"),
                "spkd_ket"          => request("spkd_ket"),
                "spkd_ref_nama"     => request("spkd_ref_nama"),
                "spkd_ref_an"     => request("spkd_ref_an"),
                "spkd_ref_alamat"     => request("spkd_ref_alamat"),
                "spkd_ref_hubungan"     => request("spkd_ref_hubungan"),
                "spkd_ref_rekening"     => request("spkd_ref_rekening"),
                "spkd_ref_telp"     => request("spkd_ref_telp"),
            ]);
        } else {
            $insert = DB::table('tb_spk_diskon')-> where("spkd_spk", request("spk_id"))->update([
                "spkd_spk"          => request("spk_id"),
                "spkd_cashback"     => str_replace(".", "", request("spkd_cashback")),
                "spkd_komisi"       => request("spkd_komisi"),
                "spkd_ket"          => request("spkd_ket"),
                "spkd_ref_nama"     => request("spkd_ref_nama"),
                "spkd_ref_an"     => request("spkd_ref_an"),
                "spkd_ref_alamat"     => request("spkd_ref_alamat"),
                "spkd_ref_hubungan"     => request("spkd_ref_hubungan"),
                "spkd_ref_rekening"     => request("spkd_ref_rekening"),
                "spkd_ref_telp"     => request("spkd_ref_telp"),
            ]);
        }

        $result =  DB::table('tb_spk_diskon')-> where("spkd_spk", request("spk_id"))->get();
        if (count($result) > 0){
            $proses = 1;
        } else {
            $proses = 0;
        }


        return $proses;
    }

	function app(){
    	$this->validate(request(), [
            "diskon_id"      	=> "required",
            "diskon_status"      => "required"
        ]);

        
        $insert = array(
             "spkd_cashback"     	=>  request("diskon_cashback"),
	        "spkd_komisi"     		=>  request("diskon_komisi"),
	        "spkd_status"     		=>  request("diskon_status"),
	        "spkd_catatan"     		=>  request("diskon_catatan"),
        );

        $id = DB::table('tb_spk_aksesoris')-> insertGetId($insert,'diskon_id');
        return json_encode(DB::table('tb_spk_aksesoris') -> where("diskon_id", $id)->first());

     }
}

