<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\CMO;

class ApiCMOController extends Controller
{
    function index(){
    	$data = DB::table('tb_cmo')
                ->get();
		$result = $data->filter(function ($data) {
		    return 
		    	(!request("cmo_nama") || strrpos(strtolower($data->cmo_nama), strtolower(request("cmo_nama"))) > -1) &&
				(!request("cmo_alamat") || strrpos(strtolower($data->cmo_alamat), strtolower(request("cmo_alamat"))) > -1) &&
				(!request("cmo_kota") || strrpos(strtolower($data->cmo_kota), strtolower(request("cmo_kota"))) > -1) &&
				(!request("cmo_telp") || strrpos(strtolower($data->cmo_telp), strtolower(request("cmo_telp"))) > -1) && 
				(!request("cmo_leasing") || $data->cmo_leasing==request("cmo_leasing")) ;
		});

    	return json_encode($result);
    }

    function store(){
    	$this->validate(request(), [
            "cmo_nama"     => "required",
            "cmo_leasing"  => "required",
            "cmo_kota"     => "required",
            "cmo_telp"     => "required"
        ]);

        
       $insert = array(
            "cmo_nama"     =>  request("cmo_nama"),
            "cmo_leasing"     =>  request("cmo_leasing"),
            "cmo_kota"           =>  request("cmo_kota"),
            "cmo_alamat"           =>  request("cmo_alamat"),
            "cmo_telp"          =>  request("cmo_telp")
        );

        $id = DB::table('tb_cmo')-> insertGetId($insert, 'cmo_id');
        return json_encode (DB::table('tb_cmo')->where("cmo_id",$id)->first());
    }

    function update(){
    	$this->validate(request(), [
            "cmo_nama"      => "required",
            "cmo_leasing"      => "required",
            "cmo_kota"     => "required",
            "cmo_telp"     => "required"
        ]);

	    DB::table('tb_cmo')->where("cmo_id",request("cmo_id"))->update([
	        "cmo_nama"     		=>  request("cmo_nama"),
            "cmo_leasing"     		=>  request("cmo_leasing"),
            "cmo_kota"          =>  request("cmo_kota"),
            "cmo_alamat"        =>  request("cmo_alamat"),
            "cmo_telp"          =>  request("cmo_telp")
	    ]);

	    return json_encode(DB::table('tb_cmo')->where("cmo_id",request("cmo_id"))->first());
    }

    function destroy(){
		return DB::table('tb_cmo')->where('cmo_id', request("cmo_id"))->delete();
    }
}
