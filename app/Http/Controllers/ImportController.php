<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Excel;

class ImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('import');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!empty($request->file('import_file'))) { 
            $path = $request->file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();

            if(!empty($data) && $data->count()){
                $tr_kendaraan = DB::table('tb_tr_kendaraan')->orderBy('trk_id', 'desc')->first();
                $no = ($tr_kendaraan != NULL ? $tr_kendaraan->trk_id : NULL);

                $no_faktur = 'no._faktur';
                $tgl_faktur = 'tgl._faktur';
                $no_mesin = 'no._mesin';

                foreach ($data as $key => $value) {
                    $no++;
                    $vendor = DB::table('tb_vendor')->where('vendor_nama', $value->branch_description)->first();
                    $variant = DB::table('tb_variant')->where('variant_serial', $value->tipe_kendaraan)->first();
                    $warna = DB::table('tb_warna')->where('warna_nama', $value->color_description)->first();

                    $insert[] = [
                        'trk_ref' => "P-" . sprintf("%05s", $no),
                        'trk_vendor' => ($vendor != NULL ? $vendor->vendor_id : NULL),
                        'trk_invoice' => $value->$no_faktur,
                        'trk_tgl' => $value->$tgl_faktur,
                        'trk_dh' => '2017000'.$no,
                        'trk_variantid' => ($variant != NULL ? $variant->variant_id : NULL),
                        'trk_warna' => ($warna != NULL ? $warna->warna_id : NULL),
                        'trk_tahun' => $value->construction_year,
                        'trk_rangka' => $value->chasis,
                        'trk_mesin' => $value->$no_mesin,
                        'trk_hrg_unit' => $value->harga_unit,
                        'trk_hrg_diskon' => $value->harga_discount,
                        'trk_hrg_pbm' => $value->harga_pbm,
                        'trk_hrg_dpp' => $value->harga_dpp,
                        'trk_hrg_ppn' => $value->harga_ppn,
                        'trk_hrg_total' => $value->harga_total_ar,
                        'trk_tgl_tempo' => $value->tanggal_jatuh_tempo,
                        'trk_interest' => $value->interest,
                        'trk_promes' => $value->promes,
                        'trk_tgl_fisik' => $value->grfisikdate,
                        'created_at' => date('Y-m-d h:i:s')
                        // '' => $value->company_code,
                        // '' => $value->branch_code,
                        // '' => $value->branch_description,
                        // '' => $value->filling_number,
                        // '' => $value->,
                        // '' => $value->tipe_kendaraan,
                        // '' => $value->color_description,
                        // 'trk_mesin' => $value->,
                    ];
                }

                if(!empty($insert)){
                    DB::table('tb_tr_kendaraan')->insert($insert);
                    dd('Insert Record successfully.');
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
