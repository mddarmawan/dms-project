<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Asuransi;
use App\Biro;

class ApiBiroController extends Controller
{
         function index(){
    	$data = DB::table('tb_biro')
                -> get();
		$result = $data->filter(function ($data) {
		    return 
		    	(!request("biro_nama") || strrpos(strtolower($data->biro_nama), strtolower(request("biro_nama"))) > -1) &&
				 (!request("biro_alamat") || strrpos(strtolower($data->biro_alamat), strtolower(request("biro_alamat"))) > -1) &&
				 (!request("biro_telp") || strrpos(strtolower($data->biro_telp), strtolower(request("biro_telp"))) > -1) &&
				 (!request("biro_kodepos") || strrpos(strtolower($data->biro_kodepos), strtolower(request("biro_kodepos"))) > -1) &&
				 (!request("biro_kota") || strrpos(strtolower($data->biro_kota), strtolower(request("biro_kota"))) > -1) &&
				 (!request("biro_email") || strrpos(strtolower($data->biro_email), strtolower(request("biro_email"))) > -1);
		});

    	return json_encode($result);
    }

    function store(){
    	$this->validate(request(), [
            "biro_nama"     	=> "required",
            "biro_telp"     	=> "required",
            "biro_kota"     	=> "required"
        ]);

        
        $insert=array(
            "biro_nama"     =>  request("biro_nama"),
            "biro_alamat"     =>  request("biro_alamat"),
            "biro_telp"           =>  request("biro_telp"),
            "biro_kota"           =>  request("biro_kota"),
            "biro_kodepos"          =>  request("biro_kodepos"),
            "biro_email"          =>  request("biro_email")
        );
        $id = DB::table('tb_biro')->insertGetId($insert,'biro_id');
        return json_encode(DB::table('tb_biro')->where("biro_id",$id)->first());
    }

    function update(){
    	$this->validate(request(), [
            "biro_nama"     	=> "required",
            "biro_telp"     	=> "required",
            "biro_kota"     		=> "required"
        ]);

	   DB::table('tb_biro')->where("biro_id",request("biro_id"))->update([
	         "biro_nama"     =>  request("biro_nama"),
            "biro_alamat"     =>  request("biro_alamat"),
            "biro_telp"           =>  request("biro_telp"),
            "biro_kota"           =>  request("biro_kota"),
            "biro_kodepos"          =>  request("biro_kodepos"),
            "biro_email"          =>  request("biro_email")
	    ]);

	    return json_encode(DB::table('tb_biro')->where("biro_id",request("biro_id"))->first());
    }

    function destroy(){
		return DB::table('tb_biro')->where('biro_id', request("biro_id"))->delete();
    }  
}
