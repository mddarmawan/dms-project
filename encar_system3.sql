-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2017 at 01:13 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `encar_system3`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2017_04_10_040338_create_ekspedisi_table', 1),
(5, '2017_04_10_071514_create_bank_kategori_table', 2),
(6, '2017_04_10_071640_create_bank_table', 2),
(7, '2017_04_10_082236_create_users_table', 3),
(8, '2017_04_13_084447_create_tb_leashing', 4),
(9, '2017_04_14_132038_create_cmo_table', 5),
(10, '2017_04_15_050321_create_spk_table', 6),
(11, '2017_04_15_053403_create_warna_table', 6),
(12, '2017_04_15_053525_create_pelanggan_table', 6),
(13, '2017_04_15_055203_create_v_spk_table', 7),
(14, '2017_04_15_055219_create_v_diskon_table', 7),
(15, '2017_04_18_041034_create_tb_variant_table', 7),
(16, '2017_04_18_041535_create_tb_type_table', 7),
(17, '2017_04_15_054946_create_tb_refferal_table', 8),
(18, '2017_04_18_053445_create_tb_jabatan', 9),
(19, '2017_04_18_053509_create_tb_karyawan', 9),
(20, '2017_04_18_053530_create_tb_team', 9),
(21, '2017_04_18_053545_create_tb_sales', 9),
(22, '2017_04_19_161422_create_tb_spk_faktur_table', 10),
(23, '2017_04_19_162741_create_tb_spk_pembayaran_table', 10),
(24, '2017_04_19_163433_create_tb_spk_leashing_table', 10),
(25, '2017_04_15_054101_create_tr_kendaraan_table', 11),
(26, '2017_04_25_060551_create_tb_spk_diskon_table', 11),
(28, '2017_04_25_060607_create_tb_spk_aksesoris_table', 12),
(29, '2017_04_25_064823_create_tb_aksesoris_table', 12),
(30, '2017_04_28_061346_create_tb_vendor_table', 13),
(31, '2017_05_11_102305_create_table_spk_referral', 14),
(35, '2017_05_11_155657_create_table_spk_ttbj', 15),
(36, '2017_05_11_170712_create_table_biro', 16),
(37, '2017_05_22_074206_create_tb_vendorAks_table', 17),
(38, '2017_05_17_080957_create_tb_asuransi_table', 18),
(40, '2017_06_11_133148_create_table_leasing_bayar', 19),
(42, '2017_06_12_051709_create_table_gudang', 20),
(43, '2017_06_14_160043_create_table_asuransi_jenis', 21),
(44, '2017_07_05_042258_create_stockmove_table', 22);

-- --------------------------------------------------------

--
-- Table structure for table `tb_aksesoris`
--

CREATE TABLE `tb_aksesoris` (
  `aksesoris_id` int(10) UNSIGNED NOT NULL,
  `aksesoris_vendor` int(11) NOT NULL,
  `aksesoris_kendaraan` int(11) DEFAULT NULL,
  `aksesoris_kode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aksesoris_nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aksesoris_harga` bigint(20) NOT NULL,
  `aksesoris_status` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_aksesoris`
--

INSERT INTO `tb_aksesoris` (`aksesoris_id`, `aksesoris_vendor`, `aksesoris_kendaraan`, `aksesoris_kode`, `aksesoris_nama`, `aksesoris_harga`, `aksesoris_status`, `created_at`, `updated_at`) VALUES
(18, 2, 5, 'AY0003', 'Sill plate Samping dan Belakang Rubber Luxury Daihatsu Sigra', 33, 1, '2017-08-11 03:34:12', '2017-08-11 04:07:53'),
(20, 2, 9, 'AY0004', 'Over Fender Offroad/Spakbor Karet Daihatsu Ayla', 600000, 1, '2017-08-11 03:35:44', '2017-08-11 03:54:18'),
(21, 2, 6, 'AY0005', 'Automatic Retractable/Modul Retracktable Spion Daihatsu Ayla/Aygo', 1395000, 1, '2017-08-11 03:36:24', '2017-08-11 04:08:02'),
(22, 2, 6, 'AY0006', 'Stop Lamp Belakang/Rear/Tail Lamp/Light Variasi Daihatsu Ayla', 1600000, 1, '2017-08-11 03:37:07', '2017-08-11 04:08:07'),
(23, 2, 6, 'AY0007', 'Cover Spion/Mirror Cover Luxury Mobil Daihatsu Ayla Type D', 250000, 1, '2017-08-11 03:37:51', '2017-08-11 04:08:12'),
(24, 2, 6, 'AY0008', 'Outer dan Door Handle Polos JSL Daihatsu Ayla', 85000, 1, '2017-08-11 03:38:13', '2017-08-11 04:08:16'),
(25, 2, 6, 'AY0009', 'Karpet Dasar/Lantai Tebal Peredam Daihatsu Ayla', 175000, 1, '2017-08-11 03:38:36', '2017-08-11 04:08:22'),
(26, 2, 6, 'AY0010', 'Garnish/Cover Wiper Luxury Daihatsu Ayla', 120000, 1, '2017-08-11 03:39:00', '2017-08-11 04:08:26'),
(27, 3, 5, 'SG0001', 'Trunklid / Trunk Lid Belakang Mobil Daihatsu Sigra Model Setengah', 160000, 1, '2017-08-11 03:39:34', '2017-08-11 04:08:43'),
(28, 3, 5, 'SG0002', 'Talang Air / Side Visor / Door Visor Slim Daihatsu Sigra', 120000, 1, '2017-08-11 03:40:04', '2017-08-11 04:08:51'),
(29, 3, 5, 'SG0003', 'Pengaman / Tanduk / Injakan / Foot Step Belakang Daihatsu Sigra', 400000, 1, '2017-08-11 03:40:32', '2017-08-11 04:08:55'),
(30, 3, 5, 'SG0004', 'Ring Bumper Luxury Chrome Daihatsu Sigra', 335000, 1, '2017-08-11 03:40:59', '2017-08-11 04:09:09'),
(31, 3, 5, 'SG0005', 'List Grill Bumper Luxury Chrome Daihatsu Sigra', 335000, 1, '2017-08-11 03:41:50', '2017-08-11 04:09:13'),
(32, 2, 2, 'XN-0001', 'Mata Sipit / Mata Cipit / Eyelid / Alis Lampu Depan Fiber Luxury Daihatsu All New Xenia', 225000, 1, '2017-08-11 03:42:31', '2017-08-11 04:09:23'),
(33, 2, 2, 'XN-0002', 'Console Box / Armrest / Arm Rest Mobil Daihatsu All New Xenia', 525000, 1, '2017-08-11 03:42:58', '2017-08-11 04:09:32'),
(34, 2, 2, 'XN-0003', 'Bumper Towing Bar Belakang Daihatsu All New Xenia', 1000000, 1, '2017-08-11 03:43:24', '2017-08-11 04:09:39'),
(35, 2, 2, 'XN-0004', 'Roof Console/Cookpit Mobil Daihatsu All New Xenia', 170000, 1, '2017-08-11 03:43:59', '2017-08-11 04:09:42'),
(36, 2, 4, 'GM-0001', 'Karpet Dasar/Lantai Tebal Peredam Daihatsu Gran Max', 215000, 1, '2017-08-11 03:45:17', '2017-08-11 04:09:47'),
(37, 2, 4, 'GM-0002', 'Valen Sport Damper/Shock Stabilizer/Absorber Protection Daihatsu Gran Max', 530000, 1, '2017-08-11 03:45:59', '2017-08-11 04:09:51'),
(43, 2, 8, 'GM-0003', 'Kunci Stir Mobil Daihatsu Gran Max Model Multy Hook Stir', 185000, 1, '2017-08-11 04:10:59', '2017-08-11 04:10:59');

-- --------------------------------------------------------

--
-- Table structure for table `tb_asuransi`
--

CREATE TABLE `tb_asuransi` (
  `asuransi_id` int(10) UNSIGNED NOT NULL,
  `asuransi_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asuransi_telp` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asuransi_email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asuransi_alamat` text COLLATE utf8mb4_unicode_ci,
  `asuransi_keterangan` text COLLATE utf8mb4_unicode_ci,
  `asuransi_status` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_asuransi`
--

INSERT INTO `tb_asuransi` (`asuransi_id`, `asuransi_nama`, `asuransi_telp`, `asuransi_email`, `asuransi_alamat`, `asuransi_keterangan`, `asuransi_status`, `created_at`, `updated_at`) VALUES
(5, 'PT. Asuransi Raksa Pratikara', '(0711) 368811', 'reksa@gmail.com', 'Jl. Radial No. 129/2878 24 Ilir, Bukit Kecil, 24 Ilir, Bukit Kecil, Palembang City, South Sumatra 30134', 'http://www.araksa.com', 1, '2017-08-11 03:10:01', '2017-08-11 03:10:01'),
(6, 'Garda Oto', '(0711) 351900', 'support@gardaoto.com', 'Jl. Veteran No.2, 9 Ilir, Ilir Tim. II, Kota Palembang, Sumatera Selatan 30114', NULL, 1, '2017-08-11 03:11:19', '2017-08-11 03:11:19'),
(7, 'PT. Asuransi Sinar Mas', '(0711) 316966', 'cs@sinarmas.co.id', 'Jl. Jend Sudirman No 2937 I-J, Kelurahan 20 Ilir II Kecamatan Ilir Timur I, 20 Ilir D. III, Ilir Timur I, Palembang City, South Sumatra 30129', NULL, 1, '2017-08-11 03:12:00', '2017-08-11 03:12:00'),
(8, 'Asuransi Mitra Maparya. PT', '(0711) 446084', 'support@mitramaparya.co.id', 'Jl. Demang Lebar Daun No.3, Demang Lebar Daun, Ilir Bar. I, Kota Palembang, Sumatera Selatan 30137', 'ini adalah keterangan', 1, '2017-08-11 03:13:00', '2017-08-11 03:13:00'),
(9, 'Adira Insurance', '1 500 456', 'leasing@adira.co.id', 'Jl. Residen Abdul Rozak, Bukit Sangkal, Kalidoni, Kota Palembang, Sumatera Selatan 30163', NULL, 1, '2017-08-11 03:13:56', '2017-08-11 03:13:56');

-- --------------------------------------------------------

--
-- Table structure for table `tb_asuransi_jenis`
--

CREATE TABLE `tb_asuransi_jenis` (
  `ajenis_id` int(10) UNSIGNED NOT NULL,
  `ajenis_nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_asuransi_jenis`
--

INSERT INTO `tb_asuransi_jenis` (`ajenis_id`, `ajenis_nama`) VALUES
(1, 'ALL RISK'),
(2, 'TLO'),
(3, 'KOMBINASI');

-- --------------------------------------------------------

--
-- Table structure for table `tb_bank`
--

CREATE TABLE `tb_bank` (
  `bank_id` int(10) UNSIGNED NOT NULL,
  `bank_kategori` int(11) NOT NULL,
  `bank_an` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_rek` int(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_bank_kategori`
--

CREATE TABLE `tb_bank_kategori` (
  `kategori_id` int(10) UNSIGNED NOT NULL,
  `kategori_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_bank_kategori`
--

INSERT INTO `tb_bank_kategori` (`kategori_id`, `kategori_nama`) VALUES
(1, 'BCA'),
(2, 'Permata');

-- --------------------------------------------------------

--
-- Table structure for table `tb_biro`
--

CREATE TABLE `tb_biro` (
  `biro_id` int(10) UNSIGNED NOT NULL,
  `biro_nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biro_alamat` text COLLATE utf8mb4_unicode_ci,
  `biro_kodepos` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biro_kota` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biro_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biro_email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_biro`
--

INSERT INTO `tb_biro` (`biro_id`, `biro_nama`, `biro_alamat`, `biro_kodepos`, `biro_kota`, `biro_telp`, `biro_email`, `created_at`, `updated_at`) VALUES
(6, 'SAMSAT', 'Jl. Kampus', '39920', 'Palembang', '0711-38828', 'samsat@gmail.com', '2017-08-11 10:43:00', '2017-08-11 10:43:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_cmo`
--

CREATE TABLE `tb_cmo` (
  `cmo_id` int(10) UNSIGNED NOT NULL,
  `cmo_leasing` int(11) NOT NULL,
  `cmo_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cmo_alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cmo_kota` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cmo_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_config`
--

CREATE TABLE `tb_config` (
  `config_id` int(11) NOT NULL,
  `config_name` varchar(50) DEFAULT NULL,
  `config_value` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_config`
--

INSERT INTO `tb_config` (`config_id`, `config_name`, `config_value`) VALUES
(1, 'logo', NULL),
(2, 'company_name', NULL),
(3, 'company_address', NULL),
(4, 'company_phone', NULL),
(5, 'company_fax', NULL),
(6, 'company_email', NULL),
(7, 'match_limit_personal', '3500000'),
(8, 'match_limit_fleet', '0'),
(9, 'branch_code', '1061'),
(10, 'company_city', NULL),
(11, 'company_province', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_ekspedisi`
--

CREATE TABLE `tb_ekspedisi` (
  `ekspedisi_id` int(10) UNSIGNED NOT NULL,
  `ekspedisi_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ekspedisi_alamat` text COLLATE utf8mb4_unicode_ci,
  `ekspedisi_kota` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ekspedisi_kodepos` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ekspedisi_email` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ekspedisi_telepon` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_ekspedisi`
--

INSERT INTO `tb_ekspedisi` (`ekspedisi_id`, `ekspedisi_nama`, `ekspedisi_alamat`, `ekspedisi_kota`, `ekspedisi_kodepos`, `ekspedisi_email`, `ekspedisi_telepon`, `created_at`, `updated_at`) VALUES
(4, 'PT. ANTAR PULAU', 'Jl. angkasa III', 'Palembang', '38882', 'support@antarpulau.co.id', '711888871', '2017-08-11 03:20:26', '2017-08-11 03:20:26'),
(5, 'PT. Cargonesia Utama Trans', 'Jl. Pos Pengumben Raya No. 13 B', 'Jakarta', '1200', 'cs@cargonesia.co.id', '2153665404', '2017-08-11 03:21:36', '2017-08-11 03:21:36'),
(6, 'PT. MULTI ANDALAN SEJAHTERA', 'JL. CAKUNG CILINCING, NO. 50, KEL. CAKUNG BARAT, KEC. CAKUNG, JAKARTA TIMUR, 13910', 'Jakarta', '13910', 'support@muldiandalan.co.id', '2129844941', '2017-08-11 03:24:09', '2017-08-11 03:24:09');

-- --------------------------------------------------------

--
-- Table structure for table `tb_gudang`
--

CREATE TABLE `tb_gudang` (
  `gudang_id` int(10) UNSIGNED NOT NULL,
  `gudang_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gudang_alamat` text COLLATE utf8mb4_unicode_ci,
  `gudang_kota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gudang_telp` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gudang_email` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_jabatan`
--

CREATE TABLE `tb_jabatan` (
  `jabatan_id` int(10) UNSIGNED NOT NULL,
  `jabatan_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_jabatan`
--

INSERT INTO `tb_jabatan` (`jabatan_id`, `jabatan_nama`, `created_at`, `updated_at`) VALUES
(1, 'Sales', '2017-04-18 06:19:01', '2017-04-18 06:19:45'),
(2, 'Admin Keuangan', '2017-04-18 06:19:26', '2017-04-25 17:00:00'),
(3, 'Supervisor', '2017-04-11 17:00:00', '2017-04-20 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_karyawan`
--

CREATE TABLE `tb_karyawan` (
  `karyawan_id` int(10) UNSIGNED NOT NULL,
  `karyawan_nip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `karyawan_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `karyawan_jabatan` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_karyawan`
--

INSERT INTO `tb_karyawan` (`karyawan_id`, `karyawan_nip`, `karyawan_nama`, `karyawan_jabatan`, `created_at`, `updated_at`) VALUES
(1, 'P-009', 'Okta Reza', 1, '2017-04-02 17:00:00', '2017-04-20 17:00:00'),
(2, 'P-012', 'Deny Ramdhan', 1, '2017-04-09 17:00:00', '2017-04-27 17:00:00'),
(3, 'P-005', 'Windi Dwi Lestari', 1, '2017-07-20 05:50:39', '2017-07-20 05:50:39'),
(4, 'P-006', 'Nilam Permata Sari', 1, '2017-07-20 05:50:39', '2017-07-20 05:50:39'),
(5, 'P-007', 'Uba Kiri Kanan', 1, '2017-07-20 05:51:59', '2017-07-20 05:51:59'),
(6, 'P-008', 'Mang Boy', 1, '2017-07-20 05:51:59', '2017-07-20 05:51:59');

-- --------------------------------------------------------

--
-- Table structure for table `tb_leasing`
--

CREATE TABLE `tb_leasing` (
  `leasing_id` int(10) UNSIGNED NOT NULL,
  `leasing_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `leasing_nick` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `leasing_alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `leasing_kota` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `leasing_telp` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `leasing_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_leasing`
--

INSERT INTO `tb_leasing` (`leasing_id`, `leasing_nama`, `leasing_nick`, `leasing_alamat`, `leasing_kota`, `leasing_telp`, `leasing_status`, `created_at`, `updated_at`) VALUES
(8, 'Dipo Star Finance', 'DSF', 'Jalan Jendral Sudirman No.132C, 20 Ilir I, Ilir Timur I, 20 Ilir D. I, Ilir Tim. I, Kota Palembang, Sumatera Selatan 30126', 'Palembang', '711353955', 1, '2017-08-11 03:01:44', '2017-08-11 03:01:44'),
(9, 'Adira Finance', 'AF', 'Jl. Jend. Sudirman, Ario Kemuning, Kemuning, Kota Palembang, Sumatera Selatan 30128', 'Palembang', '7115612793', 1, '2017-08-11 03:02:30', '2017-08-11 03:02:30'),
(10, 'Mandiri Tunas Finance', 'MTF', 'JL. Veteran, Ruko Rajawali No.931-932, Palembang, Kepandean Baru, Ilir Timur I, Palembang City, South Sumatra 30111', 'Palembang', '711378476', 1, '2017-08-11 03:03:59', '2017-08-11 03:03:59'),
(11, 'Sinar Mas', 'SM', 'Jalan R Sukamto Komplek Ruko PTC Blok H No 1, 8 Ilir, Ilir Tim. II, Kota Palembang, Sumatera Selatan 30164', 'Palembang', '711379758', 1, '2017-08-11 03:05:15', '2017-08-11 03:05:15'),
(12, 'Bess Finance', 'BF', 'Jalan Sumpah Pemuda Blok I No.7 - 7A, Lorok Pakjo, Ilir Barat I, Lorok Pakjo, Ilir Bar. I, Kota Palembang, Sumatera Selatan 30137', 'Palembang', '711311828', 1, '2017-08-11 03:05:56', '2017-08-11 03:05:56'),
(13, 'BAF', 'BAF', 'Jl. Jend. Sudirman, Ario Kemuning, Kemuning, Kota Palembang, Sumatera Selatan 30128', 'Palembang', '7115610702', 1, '2017-08-11 03:06:30', '2017-08-11 03:06:30'),
(14, 'Indomobil Finance', 'IF', 'Jl. Basuki Rahmat No. 24F, RT. 024/009, Pahlawan, Kemuning, Pahlawan, Kemuning, Kota Palembang, Sumatera Selatan 30127', 'Palembang', '711319934', 1, '2017-08-11 03:07:01', '2017-08-11 03:07:01'),
(15, 'Mitsui Leasing', 'ML', 'Jl. Ptc No.59, 8 Ilir, Ilir Tim. II, Kota Palembang, Sumatera Selatan 30114', 'Palembang', '711382460', 1, '2017-08-11 03:08:11', '2017-08-11 03:08:11');

-- --------------------------------------------------------

--
-- Table structure for table `tb_leasing_bayar`
--

CREATE TABLE `tb_leasing_bayar` (
  `lbayar_id` int(10) UNSIGNED NOT NULL,
  `lbayar_spkl` int(11) NOT NULL,
  `lbayar_tgl` date NOT NULL,
  `lbayar_nominal` bigint(20) NOT NULL,
  `lbayar_ket` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_leasing_bayar`
--

INSERT INTO `tb_leasing_bayar` (`lbayar_id`, `lbayar_spkl`, `lbayar_tgl`, `lbayar_nominal`, `lbayar_ket`, `created_at`, `updated_at`) VALUES
(1, 6, '2017-08-12', 100000000, 'DP ', NULL, NULL),
(2, 6, '2017-08-12', 100000000, 'Pelunasan', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pelanggan`
--

CREATE TABLE `tb_pelanggan` (
  `pel_id` int(10) UNSIGNED NOT NULL,
  `pel_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pel_identitas` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pel_alamat` text COLLATE utf8mb4_unicode_ci,
  `pel_pos` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pel_kota` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pel_email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pel_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pel_ponsel` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pel_lahir` date DEFAULT NULL,
  `pel_sales` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_pelanggan`
--

INSERT INTO `tb_pelanggan` (`pel_id`, `pel_nama`, `pel_identitas`, `pel_alamat`, `pel_pos`, `pel_kota`, `pel_email`, `pel_telp`, `pel_ponsel`, `pel_lahir`, `pel_sales`, `created_at`, `updated_at`) VALUES
(48, 'Rahmat Latif', '1671022809900009', 'jl. kh wahid hasyim lr berdikari', '30257', 'Palembang', 'rahmatlatif@gmail.com', '0', '081369342713', '1990-09-28', 4, '2017-08-11 02:44:12', '2017-08-11 20:04:09'),
(49, 'Windi Dwi Lestari', '3303884730001', 'Jl. Dempo Raya Ilir Barat I', '30344', 'Palembang', 'windidwilestari@gmail.com', '0711-899919', '08787777888', '1992-02-03', 4, '2017-08-11 02:53:47', '2017-08-11 02:53:47'),
(50, 'Mang Cek Boy', '109912211011', 'Plaju Ulu', '301192', 'Palembang', 'haaha@gmail.com', '07118817211', '089987721311', '1977-08-11', 1, '2017-08-11 03:04:38', '2017-08-11 03:04:38'),
(51, 'Rahmat Siagian', '33031778920001', 'Jl. Musi Raya Barat, Baung IV no.236', '30163', 'Palembang', 'rahmat@gmail.com', '0711823888', '08131111010', '1990-07-12', 4, '2017-08-11 03:50:38', '2017-08-11 03:51:00'),
(52, 'Muhammad Yusuf Gumay', '33031132850003', 'Jl. Nibung 4 no.98', '30167', 'Palembang', 'ucupz@gmail.com', '0711-882781', '08527777758', '1991-10-17', 2, '2017-08-11 03:54:00', '2017-08-11 03:54:00'),
(53, 'Ardie Wiratama Cibay', '1671012919900002', 'Komp Pusri Sako', '301991', 'Palembang', 'ardicibay@gmail.com', '0711899281', '081344552582', '1993-08-11', 6, '2017-08-11 03:54:59', '2017-08-11 03:54:59'),
(54, 'CV. Karya Siber Indonesia', '017087098374', 'Jl. Prajurit KMS Ali', '30111', 'Palembang', 'karyasiber@gmail.com', '0711-700012', '0852776685', '2017-01-01', 1, '2017-08-11 04:14:54', '2017-08-11 04:14:54'),
(55, 'Fajar Fernanda', '66067772380001', 'Jl. Trikora 2 No.89 RT. 09 RW.', '400142', 'Palembang', 'fajarfernanda@gmail.com', '0711-774812', '081300090009', '1978-03-14', 1, '2017-08-11 05:22:19', '2017-08-11 05:23:23'),
(56, 'Tegar Purnama', '88900029838001', 'Jl. Tanjung Api api', '30090', 'Palembang', 'tegarpurnama@gmail.com', '0711-788782', '0898200200', '1990-10-19', 3, '2017-08-11 10:12:10', '2017-08-11 10:12:27'),
(57, 'DONI DARMAWAN', '98848923929', 'JL. SULTAN', '39992', 'PALEMBANG', 'donidarmawan@gmail.com', '0711-239919', '0821777482', '1988-08-11', 5, '2017-08-11 10:49:58', '2017-08-11 10:49:58'),
(58, 'qweqweq', '123123', 'qweqweqw', '123123', 'wqeqweq', 'qweqwe', '12312', '1231231', '2017-08-12', 3, '2017-08-11 20:05:22', '2017-08-11 20:05:22'),
(59, 'DICKY WIJAYA KUSUMA', '1671101408750010', 'Jl. TAKWA LORONG JAKARTA PALEMBANG', '30020', 'PALEMBANG', 'DICKYWIJAYA@GMAIL.COM', '0711-822282', '085279574682', '1975-08-14', 3, '2017-08-11 20:33:43', '2017-08-11 20:34:06');

-- --------------------------------------------------------

--
-- Table structure for table `tb_referral`
--

CREATE TABLE `tb_referral` (
  `referral_id` int(10) UNSIGNED NOT NULL,
  `referral_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referral_alamat` text COLLATE utf8mb4_unicode_ci,
  `referral_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referral_bank` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referral_rek` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referral_an` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_referral`
--

INSERT INTO `tb_referral` (`referral_id`, `referral_nama`, `referral_alamat`, `referral_telp`, `referral_bank`, `referral_rek`, `referral_an`, `created_at`, `updated_at`) VALUES
(7, 'Harry Purmanta Siagian', 'Jalan Baung', '08115123441', 'BCA', '1233221', 'Harry Purmanta Siagian', '2017-08-11 02:46:12', '2017-08-11 02:46:12'),
(8, 'Rahmi Liza', 'Jl. Bukit Lama', '085200002391', 'BNI', '0178305704', 'Rahmi Liza', '2017-08-11 02:46:30', '2017-08-11 02:46:30'),
(9, 'Alek Santoso', 'Kertapati', '08992348112', 'BNI', '1122131141', 'Alek Santoso', '2017-08-11 02:46:49', '2017-08-11 02:46:49'),
(10, 'Nurdin', 'Jl. Pakjo sebelah kiri', '0813777783', 'BRI', '700601003614530', 'Nurdin', '2017-08-11 02:47:38', '2017-08-11 02:47:38'),
(11, 'Nanda Pratama Putra', 'Jl. Anggrek raya no.23', '0889222213', 'Mandiri', '1560009861578', 'Nanda Pratama Putra', '2017-08-11 02:48:50', '2017-08-11 02:48:50'),
(12, 'Mohammad Muhaimin', 'Jl. sekip pangkal', '08571988192', 'Danamon', '1430049736', 'Mohammad Muhaimin', '2017-08-11 02:50:44', '2017-08-11 02:50:44');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sales`
--

CREATE TABLE `tb_sales` (
  `sales_id` int(10) UNSIGNED NOT NULL,
  `sales_karyawan` int(10) UNSIGNED NOT NULL,
  `sales_pwd` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `karyawan_usr` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sales_team` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_sales`
--

INSERT INTO `tb_sales` (`sales_id`, `sales_karyawan`, `sales_pwd`, `karyawan_usr`, `sales_team`, `created_at`, `updated_at`) VALUES
(1, 1, 'sales', 'okta', 1, '2017-04-26 17:00:00', '2017-04-28 17:00:00'),
(2, 2, 'Zunal ', '12345', 1, '2017-07-20 04:40:01', '2017-07-20 04:40:01'),
(3, 3, '123456', 'Windi', 1, '2017-07-20 05:53:51', '2017-07-20 05:53:51'),
(4, 4, '123456', 'Nilam', 1, '2017-07-20 05:53:51', '2017-07-20 05:53:51'),
(5, 5, '123456', 'ubakiri', 1, '2017-07-20 05:54:58', '2017-07-20 05:54:58'),
(6, 6, '123456', 'mangboy', 1, '2017-07-20 05:54:58', '2017-07-20 05:54:58');

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk`
--

CREATE TABLE `tb_spk` (
  `spk_id` varchar(10) NOT NULL,
  `spk_tgl` date DEFAULT NULL,
  `spk_lat` varchar(30) DEFAULT NULL,
  `spk_lng` varchar(30) DEFAULT NULL,
  `spk_pajak` char(1) DEFAULT NULL,
  `spk_npwp` varchar(30) DEFAULT NULL,
  `spk_fleet` varchar(50) DEFAULT NULL,
  `spk_pel_nama` varchar(50) DEFAULT NULL,
  `spk_pel_identitas` varchar(20) DEFAULT NULL,
  `spk_pel_alamat` text,
  `spk_pel_provinsi` varchar(30) DEFAULT NULL,
  `spk_pel_kota` varchar(30) DEFAULT NULL,
  `spk_pel_pos` varchar(10) DEFAULT NULL,
  `spk_pel_telp` varchar(15) DEFAULT NULL,
  `spk_pel_ponsel` varchar(15) DEFAULT NULL,
  `spk_pel_email` varchar(30) DEFAULT NULL,
  `spk_pel_kategori` char(1) DEFAULT NULL,
  `spk_stnk_nama` varchar(50) DEFAULT NULL,
  `spk_stnk_identitas` varchar(20) DEFAULT NULL,
  `spk_stnk_alamat` text,
  `spk_stnk_provinsi` varchar(30) DEFAULT NULL,
  `spk_stnk_kota` varchar(30) DEFAULT NULL,
  `spk_stnk_pos` varchar(10) DEFAULT NULL,
  `spk_stnk_alamatd` text,
  `spk_stnk_provinsid` varchar(30) DEFAULT NULL,
  `spk_stnk_kotad` varchar(30) DEFAULT NULL,
  `spk_stnk_posd` varchar(10) DEFAULT NULL,
  `spk_stnk_telp` varchar(15) DEFAULT NULL,
  `spk_stnk_ponsel` varchar(15) DEFAULT NULL,
  `spk_stnk_email` varchar(30) DEFAULT NULL,
  `spk_pembayaran` char(1) DEFAULT NULL,
  `spk_leasing` int(11) DEFAULT NULL,
  `spk_kendaraan` int(11) DEFAULT NULL,
  `spk_kendaraan_harga` char(1) DEFAULT NULL,
  `spk_warna` int(11) DEFAULT NULL,
  `spk_harga` bigint(20) DEFAULT '0',
  `spk_bbn` bigint(20) DEFAULT '0',
  `spk_dh` varchar(10) DEFAULT NULL,
  `spk_match` date DEFAULT NULL,
  `spk_waktu_match` date DEFAULT NULL,
  `spk_sales` int(11) DEFAULT NULL,
  `spk_status` char(2) DEFAULT '99',
  `spk_do` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk_aksesoris`
--

CREATE TABLE `tb_spk_aksesoris` (
  `spka_diskon` int(11) NOT NULL,
  `spka_kode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spka_nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spka_harga` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_spk_aksesoris`
--

INSERT INTO `tb_spk_aksesoris` (`spka_diskon`, `spka_kode`, `spka_nama`, `spka_harga`, `created_at`, `updated_at`) VALUES
(1, 'SJ0001', 'Sarung Jok', 250000, '2017-07-20 01:13:22', '2017-07-20 01:13:22'),
(1, 'SP0001', 'Spion Xenia', 300000, '2017-07-20 01:13:23', '2017-07-20 01:13:23'),
(1, 'SJ0004', 'Sarung Jok Luxuri All Type ', 5000000, '2017-07-20 01:13:23', '2017-07-20 01:13:23'),
(2, 'SP0002', 'Spion Terios Luxury', 5000000, '2017-07-20 01:46:49', '2017-07-20 01:46:49'),
(2, 'DSO098', 'TALANG AIR ', 200000, '2017-07-20 01:46:49', '2017-07-20 01:46:49'),
(2, 'SP0002', 'Spion Terios Luxury', 5000000, '2017-07-20 01:46:49', '2017-07-20 01:46:49'),
(3, 'SJ0001', 'Sarung Jok', 250000, '2017-07-20 02:50:32', '2017-07-20 02:50:32'),
(4, 'SJ0001', 'Sarung Jok', 250000, '2017-07-20 03:18:22', '2017-07-20 03:18:22'),
(4, 'DSO098', 'TALANG AIR ', 200000, '2017-07-20 03:18:22', '2017-07-20 03:18:22'),
(5, 'SJ0001', 'Sarung Jok', 250000, '2017-07-20 04:32:05', '2017-07-20 04:32:05'),
(5, 'DSO098', 'TALANG AIR ', 200000, '2017-07-20 04:32:05', '2017-07-20 04:32:05'),
(6, 'SJ0002', 'Sarung Jok Ayla', 1500000, '2017-07-20 20:24:39', '2017-07-20 20:24:39'),
(7, 'SP0001', 'Spion Xenia', 300000, '2017-07-21 00:57:04', '2017-07-21 00:57:04'),
(7, 'SJ0001', 'Sarung Jok', 250000, '2017-07-21 00:57:04', '2017-07-21 00:57:04'),
(8, 'SJ0001', 'Sarung Jok', 250000, '2017-07-21 01:09:33', '2017-07-21 01:09:33'),
(8, 'LD0001', 'Lampu Depan Sigra ', 750000, '2017-07-21 01:09:33', '2017-07-21 01:09:33'),
(9, 'SJ0001', 'Sarung Jok', 250000, '2017-07-21 02:11:30', '2017-07-21 02:11:30'),
(25, 'LD0001', 'Lampu Depan Sigra ', 750000, '2017-07-24 03:41:39', '2017-07-24 03:41:39'),
(25, 'SJ0001', 'Sarung Jok', 250000, '2017-07-24 03:41:39', '2017-07-24 03:41:39'),
(23, 'SJ0001', 'Sarung Jok', 250000, '2017-07-24 04:16:07', '2017-07-24 04:16:07'),
(23, 'SP0001', 'Spion Xenia', 300000, '2017-07-24 04:16:07', '2017-07-24 04:16:07'),
(21, 'SP0001', 'Spion Xenia', 300000, '2017-07-24 04:18:18', '2017-07-24 04:18:18'),
(32, 'DSO098', 'TALANG AIR ', 200000, '2017-07-24 10:13:32', '2017-07-24 10:13:32'),
(37, 'SJ0004', 'Sarung Jok Luxuri All Type ', 5000000, '2017-07-24 10:14:57', '2017-07-24 10:14:57'),
(37, 'SJ0001', 'Sarung Jok', 250000, '2017-07-24 10:14:58', '2017-07-24 10:14:58'),
(43, 'DSO098', 'TALANG AIR ', 200000, '2017-07-24 10:15:25', '2017-07-24 10:15:25'),
(36, 'SJ0001', 'Sarung Jok', 250000, '2017-07-24 10:39:33', '2017-07-24 10:39:33'),
(28, 'SP0001', 'Spion Xenia', 300000, '2017-07-24 18:38:49', '2017-07-24 18:38:49'),
(28, 'SJ0001', 'Sarung Jok', 250000, '2017-07-24 18:38:49', '2017-07-24 18:38:49'),
(27, 'SJ0001', 'Sarung Jok', 250000, '2017-07-24 18:44:54', '2017-07-24 18:44:54'),
(41, 'SJ0001', 'Sarung Jok', 250000, '2017-07-24 18:51:43', '2017-07-24 18:51:43'),
(41, 'SP0001', 'Spion Xenia', 300000, '2017-07-24 18:51:43', '2017-07-24 18:51:43'),
(22, 'DSO098', 'TALANG AIR ', 200000, '2017-07-24 18:52:24', '2017-07-24 18:52:24'),
(35, 'SJ0001', 'Sarung Jok', 250000, '2017-07-24 18:52:54', '2017-07-24 18:52:54'),
(61, 'SJ0001', 'Sarung Jok', 250000, '2017-08-10 21:09:26', '2017-08-10 21:09:26'),
(61, 'LD0001', 'Lampu Depan Sigra ', 750000, '2017-08-10 21:09:26', '2017-08-10 21:09:26'),
(68, 'LD0002', 'Lampu Depan Grand Max', 540000, '2017-08-11 00:58:42', '2017-08-11 00:58:42'),
(68, 'SJ0004', 'Sarung Jok Luxuri All Type ', 5000000, '2017-08-11 00:58:42', '2017-08-11 00:58:42'),
(70, 'AY0007', 'Cover Spion/Mirror Cover Luxury Mobil Daihatsu Ayla Type D', 250000, '2017-08-11 04:02:57', '2017-08-11 04:02:57'),
(70, 'AY0007', 'Cover Spion/Mirror Cover Luxury Mobil Daihatsu Ayla Type D', 250000, '2017-08-11 04:02:57', '2017-08-11 04:02:57'),
(70, 'AY0007', 'Cover Spion/Mirror Cover Luxury Mobil Daihatsu Ayla Type D', 250000, '2017-08-11 04:02:57', '2017-08-11 04:02:57'),
(74, 'AY0007', 'Cover Spion/Mirror Cover Luxury Mobil Daihatsu Ayla Type D', 250000, '2017-08-11 04:03:30', '2017-08-11 04:03:30'),
(74, 'AY0006', 'Stop Lamp Belakang/Rear/Tail Lamp/Light Variasi Daihatsu Ayla', 1600000, '2017-08-11 04:03:30', '2017-08-11 04:03:30'),
(74, 'AY0007', 'Cover Spion/Mirror Cover Luxury Mobil Daihatsu Ayla Type D', 250000, '2017-08-11 04:03:30', '2017-08-11 04:03:30'),
(75, 'AY0003', 'Sill plate Samping dan Belakang Rubber Luxury Daihatsu Sigra', 330000, '2017-08-11 04:59:04', '2017-08-11 04:59:04'),
(75, 'AY0003', 'Sill plate Samping dan Belakang Rubber Luxury Daihatsu Sigra', 330000, '2017-08-11 04:59:04', '2017-08-11 04:59:04'),
(75, 'AY0003', 'Sill plate Samping dan Belakang Rubber Luxury Daihatsu Sigra', 330000, '2017-08-11 04:59:04', '2017-08-11 04:59:04'),
(81, 'AY0002', 'Console Box / Armrest / Arm Rest Mobil Daihatsu Ayla', 525000, '2017-08-11 20:47:04', '2017-08-11 20:47:04');

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk_diskon`
--

CREATE TABLE `tb_spk_diskon` (
  `spkd_id` int(10) UNSIGNED NOT NULL,
  `spkd_spk` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkd_cashback` bigint(20) NOT NULL,
  `spkd_komisi` bigint(20) NOT NULL,
  `spkd_ket` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkd_referral` int(11) NOT NULL,
  `spkd_status` int(1) DEFAULT NULL,
  `spkd_catatan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_spk_diskon`
--

INSERT INTO `tb_spk_diskon` (`spkd_id`, `spkd_spk`, `spkd_cashback`, `spkd_komisi`, `spkd_ket`, `spkd_referral`, `spkd_status`, `spkd_catatan`, `created_at`, `updated_at`) VALUES
(69, '17-00002', 2000000, 500000, 'cash back 2.000,000', 7, 1, 'diskon pot', '2017-08-11 02:47:37', '2017-08-11 02:53:31'),
(70, '17-00601', 3000000, 10000000, 'Terdapat cashback untuk kendaraan yang dipesan pelanggan', 10, 1, NULL, '2017-08-11 02:53:47', '2017-08-11 04:02:57'),
(71, '17-00005', 1000000, 0, 'Cash Back 1.000.000', 10, 1, NULL, '2017-08-11 03:04:38', '2017-08-11 04:21:52'),
(72, '17-00602', 1500000, 0, 'ada cashback untuk kendaraan ini', 8, 1, NULL, '2017-08-11 03:51:00', '2017-08-11 04:22:00'),
(73, '17-00603', 300000, 0, 'Cashback untuk kendaraan ini sebesar Rp. 300.000', 7, 1, NULL, '2017-08-11 03:54:00', '2017-08-11 04:21:57'),
(74, '17-0006', 5000000, 1000000, 'Cash Back 5.000.000', 10, 1, NULL, '2017-08-11 03:54:59', '2017-08-11 04:03:30'),
(75, '17-00604', 5000000, 100000, 'Perusahaan ini mendapatkan cashback sebesar Rp. 50.000.000 karena referalnya ganteng', 7, 1, NULL, '2017-08-11 04:14:54', '2017-08-11 04:59:04'),
(76, '17-00605', 1700000, 1000000, 'kendaraan ini terdapat diskon', 7, 1, 'referalnya ganteng', '2017-08-11 05:23:23', '2017-08-11 05:27:37'),
(77, '17-00622', 0, 500000, '-', 7, 1, 'tidak ada cashback', '2017-08-11 10:12:27', '2017-08-11 10:14:19'),
(78, '123111', 50000, 0, 'DAPAT CASHBACK', 9, NULL, NULL, '2017-08-11 10:49:58', '2017-08-11 10:49:58'),
(79, '17-00025', 5000000, 1000000, 'cashback 5000000', 11, 1, 'cash back 5.000.000', '2017-08-11 20:04:08', '2017-08-11 20:15:54'),
(80, '1231231', 123123, 0, 'wqeqweqw', 9, NULL, NULL, '2017-08-11 20:05:22', '2017-08-11 20:05:22'),
(81, '17-01162', 5000000, 1000000, 'INI ADALAH KETERANGAN', 7, 1, 'INI ADALAH CATATAN', '2017-08-11 20:34:06', '2017-08-11 20:47:04');

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk_faktur`
--

CREATE TABLE `tb_spk_faktur` (
  `spkf_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkf_spk` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkf_tgl` date NOT NULL,
  `spkf_cetak` int(11) DEFAULT NULL,
  `spkf_user` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_spk_faktur`
--

INSERT INTO `tb_spk_faktur` (`spkf_id`, `spkf_spk`, `spkf_tgl`, `spkf_cetak`, `spkf_user`, `created_at`, `updated_at`) VALUES
('F-20170001', '17-00002', '2017-08-11', 0, NULL, '2017-08-11 02:57:36', '2017-08-11 02:57:42'),
('F-20170002', '17-00005', '2017-08-11', 0, NULL, '2017-08-11 03:48:01', '2017-08-11 03:48:09'),
('F-20170003', '17-0006', '2017-08-11', 0, NULL, '2017-08-11 04:14:57', '2017-08-11 04:15:00'),
('F-20170004', '17-00604', '2017-08-11', 1, NULL, '2017-08-11 04:51:29', '2017-08-11 22:35:12'),
('F-20170005', '17-00622', '2017-08-11', NULL, NULL, '2017-08-11 10:29:21', '2017-08-11 10:29:21'),
('F-20170006', '17-01162', '2017-08-12', NULL, NULL, '2017-08-11 21:19:38', '2017-08-11 22:32:57');

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk_leasing`
--

CREATE TABLE `tb_spk_leasing` (
  `spkl_id` int(10) UNSIGNED NOT NULL,
  `spkl_spk` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkl_leasing` int(10) UNSIGNED NOT NULL,
  `spkl_asuransi` int(11) DEFAULT NULL,
  `spkl_jenis_asuransi` int(11) DEFAULT NULL,
  `spkl_dp` bigint(20) UNSIGNED NOT NULL,
  `spkl_droping` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkl_waktu` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spkl_angsuran` bigint(20) UNSIGNED DEFAULT NULL,
  `spkl_cetak` date DEFAULT NULL,
  `spkl_tagihan` date DEFAULT NULL,
  `spkl_lunas` date DEFAULT NULL,
  `spkl_refund` date DEFAULT NULL,
  `spkl_jumlah_refund` bigint(20) DEFAULT NULL,
  `spkl_user` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_spk_leasing`
--

INSERT INTO `tb_spk_leasing` (`spkl_id`, `spkl_spk`, `spkl_leasing`, `spkl_asuransi`, `spkl_jenis_asuransi`, `spkl_dp`, `spkl_droping`, `spkl_waktu`, `spkl_angsuran`, `spkl_cetak`, `spkl_tagihan`, `spkl_lunas`, `spkl_refund`, `spkl_jumlah_refund`, `spkl_user`, `created_at`, `updated_at`) VALUES
(5, '17-00005', 8, 6, 1, 0, '0', NULL, 0, '2017-08-11', '2017-08-11', '2017-08-11', '2017-08-11', 500000, NULL, '2017-08-11 03:47:58', '2017-08-11 04:12:22'),
(6, '17-0006', 11, 5, 1, 15000000, '200000000', '5 TH', 3750000, '2017-08-11', '2017-08-16', '2017-08-16', NULL, NULL, NULL, '2017-08-11 04:14:08', '2017-08-11 04:40:55'),
(7, '17-00604', 12, NULL, 1, 60000000, '127750000', '5 THN', 5000000, NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-11 04:47:32', '2017-08-11 22:12:44'),
(8, '17-00605', 13, NULL, NULL, 30000000, '212000000', '5 THN', 4000000, NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-11 05:38:09', '2017-08-11 05:38:09'),
(9, '17-00622', 11, 7, 1, 40000000, '64550000', '3 THN', 5000000, '2017-08-11', '2017-08-12', '2017-08-12', NULL, NULL, NULL, '2017-08-11 10:19:20', '2017-08-11 20:07:59'),
(10, '17-01162', 11, 7, 1, 41497303, '73752697', '5 THN', 5000000, '2017-08-12', '2017-08-15', '2017-08-15', '2017-08-15', 1000000, NULL, '2017-08-11 21:09:45', '2017-08-11 22:40:05');

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk_no`
--

CREATE TABLE `tb_spk_no` (
  `spkNo_id` int(10) UNSIGNED NOT NULL,
  `spk_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkNo_sales` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_spk_no`
--

INSERT INTO `tb_spk_no` (`spkNo_id`, `spk_id`, `spkNo_sales`, `created_at`, `updated_at`) VALUES
(37, '17-00001', 1, NULL, NULL),
(38, '17-00002', 1, NULL, NULL),
(39, '17-00003', 1, NULL, NULL),
(40, '17-00004', 1, NULL, NULL),
(41, '17-00005', 1, NULL, NULL),
(42, '17-00006', 1, NULL, NULL),
(43, '17-00007', 1, NULL, NULL),
(44, '17-00008', 1, NULL, NULL),
(45, '17-00009', 1, NULL, NULL),
(46, '17-00010', 1, NULL, NULL),
(47, '17-00011', 1, NULL, NULL),
(48, '17-00012', 1, NULL, NULL),
(49, '17-00013', 1, NULL, NULL),
(50, '17-00014', 1, NULL, NULL),
(51, '17-00015', 1, NULL, NULL),
(52, '17-00016', 1, NULL, NULL),
(53, '17-00017', 1, NULL, NULL),
(54, '17-00018', 1, NULL, NULL),
(55, '17-00019', 1, NULL, NULL),
(56, '17-00020', 1, NULL, NULL),
(57, '17-00021', 1, NULL, NULL),
(58, '17-00022', 1, NULL, NULL),
(59, '17-00023', 1, NULL, NULL),
(60, '17-00024', 1, NULL, NULL),
(61, '17-00025', 1, NULL, NULL),
(62, '17-00026', 1, NULL, NULL),
(63, '17-00027', 1, NULL, NULL),
(64, '17-00028', 1, NULL, NULL),
(65, '17-00029', 1, NULL, NULL),
(66, '17-00030', 1, NULL, NULL),
(67, '17-00031', 1, NULL, NULL),
(68, '17-00032', 1, NULL, NULL),
(69, '17-00033', 1, NULL, NULL),
(70, '17-00034', 1, NULL, NULL),
(71, '17-00035', 1, NULL, NULL),
(72, '17-00036', 1, NULL, NULL),
(73, '17-00037', 1, NULL, NULL),
(74, '17-00038', 1, NULL, NULL),
(75, '17-00039', 1, NULL, NULL),
(76, '17-00040', 1, NULL, NULL),
(77, '17-00041', 1, NULL, NULL),
(78, '17-00042', 1, NULL, NULL),
(79, '17-00043', 1, NULL, NULL),
(80, '17-00044', 1, NULL, NULL),
(81, '17-00045', 1, NULL, NULL),
(82, '17-00046', 1, NULL, NULL),
(83, '17-00047', 1, NULL, NULL),
(84, '17-00048', 1, NULL, NULL),
(85, '17-00049', 1, NULL, NULL),
(86, '17-00050', 1, NULL, NULL),
(87, '17-00051', 1, NULL, NULL),
(88, '17-00052', 1, NULL, NULL),
(89, '17-00053', 1, NULL, NULL),
(90, '17-00054', 1, NULL, NULL),
(91, '17-00055', 1, NULL, NULL),
(92, '17-00056', 1, NULL, NULL),
(93, '17-00057', 1, NULL, NULL),
(94, '17-00058', 1, NULL, NULL),
(95, '17-00059', 1, NULL, NULL),
(96, '17-00060', 1, NULL, NULL),
(97, '17-00061', 1, NULL, NULL),
(98, '17-00062', 1, NULL, NULL),
(99, '17-00063', 1, NULL, NULL),
(100, '17-00064', 1, NULL, NULL),
(101, '17-00065', 1, NULL, NULL),
(102, '17-00066', 1, NULL, NULL),
(103, '17-00067', 1, NULL, NULL),
(104, '17-00068', 1, NULL, NULL),
(105, '17-00069', 1, NULL, NULL),
(106, '17-00070', 1, NULL, NULL),
(107, '17-00071', 1, NULL, NULL),
(108, '17-00072', 1, NULL, NULL),
(109, '17-00073', 1, NULL, NULL),
(110, '17-00074', 1, NULL, NULL),
(111, '17-00075', 1, NULL, NULL),
(112, '17-00076', 1, NULL, NULL),
(113, '17-00077', 1, NULL, NULL),
(114, '17-00078', 1, NULL, NULL),
(115, '17-00079', 1, NULL, NULL),
(116, '17-00080', 1, NULL, NULL),
(117, '17-00081', 1, NULL, NULL),
(118, '17-00082', 1, NULL, NULL),
(119, '17-00083', 1, NULL, NULL),
(120, '17-00084', 1, NULL, NULL),
(121, '17-00085', 1, NULL, NULL),
(122, '17-00086', 1, NULL, NULL),
(123, '17-00087', 1, NULL, NULL),
(124, '17-00088', 1, NULL, NULL),
(125, '17-00089', 1, NULL, NULL),
(126, '17-00090', 1, NULL, NULL),
(127, '17-00091', 1, NULL, NULL),
(128, '17-00092', 1, NULL, NULL),
(129, '17-00093', 1, NULL, NULL),
(130, '17-00094', 1, NULL, NULL),
(131, '17-00095', 1, NULL, NULL),
(132, '17-00096', 1, NULL, NULL),
(133, '17-00097', 1, NULL, NULL),
(134, '17-00098', 1, NULL, NULL),
(135, '17-00099', 1, NULL, NULL),
(136, '17-00100', 1, NULL, NULL),
(137, '17-00001', 1, NULL, NULL),
(138, '17-00002', 1, NULL, NULL),
(139, '17-00003', 1, NULL, NULL),
(140, '17-00004', 1, NULL, NULL),
(141, '17-00005', 1, NULL, NULL),
(142, '17-00006', 1, NULL, NULL),
(143, '17-00007', 1, NULL, NULL),
(144, '17-00008', 1, NULL, NULL),
(145, '17-00009', 1, NULL, NULL),
(146, '17-00010', 1, NULL, NULL),
(147, '17-00011', 1, NULL, NULL),
(148, '17-00012', 1, NULL, NULL),
(149, '17-00013', 1, NULL, NULL),
(150, '17-00014', 1, NULL, NULL),
(151, '17-00015', 1, NULL, NULL),
(152, '17-00016', 1, NULL, NULL),
(153, '17-00017', 1, NULL, NULL),
(154, '17-00018', 1, NULL, NULL),
(155, '17-00019', 1, NULL, NULL),
(156, '17-00020', 1, NULL, NULL),
(157, '17-00021', 1, NULL, NULL),
(158, '17-00022', 1, NULL, NULL),
(159, '17-00023', 1, NULL, NULL),
(160, '17-00024', 1, NULL, NULL),
(161, '17-00025', 1, NULL, NULL),
(162, '17-00026', 1, NULL, NULL),
(163, '17-00027', 1, NULL, NULL),
(164, '17-00028', 1, NULL, NULL),
(165, '17-00029', 1, NULL, NULL),
(166, '17-00030', 1, NULL, NULL),
(167, '17-00031', 1, NULL, NULL),
(168, '17-00032', 1, NULL, NULL),
(169, '17-00033', 1, NULL, NULL),
(170, '17-00034', 1, NULL, NULL),
(171, '17-00035', 1, NULL, NULL),
(172, '17-00036', 1, NULL, NULL),
(173, '17-00037', 1, NULL, NULL),
(174, '17-00038', 1, NULL, NULL),
(175, '17-00039', 1, NULL, NULL),
(176, '17-00040', 1, NULL, NULL),
(177, '17-00041', 1, NULL, NULL),
(178, '17-00042', 1, NULL, NULL),
(179, '17-00043', 1, NULL, NULL),
(180, '17-00044', 1, NULL, NULL),
(181, '17-00045', 1, NULL, NULL),
(182, '17-00046', 1, NULL, NULL),
(183, '17-00047', 1, NULL, NULL),
(184, '17-00048', 1, NULL, NULL),
(185, '17-00049', 1, NULL, NULL),
(186, '17-00050', 1, NULL, NULL),
(187, '17-00051', 1, NULL, NULL),
(188, '17-00052', 1, NULL, NULL),
(189, '17-00053', 1, NULL, NULL),
(190, '17-00054', 1, NULL, NULL),
(191, '17-00055', 1, NULL, NULL),
(192, '17-00056', 1, NULL, NULL),
(193, '17-00057', 1, NULL, NULL),
(194, '17-00058', 1, NULL, NULL),
(195, '17-00059', 1, NULL, NULL),
(196, '17-00060', 1, NULL, NULL),
(197, '17-00061', 1, NULL, NULL),
(198, '17-00062', 1, NULL, NULL),
(199, '17-00063', 1, NULL, NULL),
(200, '17-00064', 1, NULL, NULL),
(201, '17-00065', 1, NULL, NULL),
(202, '17-00066', 1, NULL, NULL),
(203, '17-00067', 1, NULL, NULL),
(204, '17-00068', 1, NULL, NULL),
(205, '17-00069', 1, NULL, NULL),
(206, '17-00070', 1, NULL, NULL),
(207, '17-00071', 1, NULL, NULL),
(208, '17-00072', 1, NULL, NULL),
(209, '17-00073', 1, NULL, NULL),
(210, '17-00074', 1, NULL, NULL),
(211, '17-00075', 1, NULL, NULL),
(212, '17-00076', 1, NULL, NULL),
(213, '17-00077', 1, NULL, NULL),
(214, '17-00078', 1, NULL, NULL),
(215, '17-00079', 1, NULL, NULL),
(216, '17-00080', 1, NULL, NULL),
(217, '17-00081', 1, NULL, NULL),
(218, '17-00082', 1, NULL, NULL),
(219, '17-00083', 1, NULL, NULL),
(220, '17-00084', 1, NULL, NULL),
(221, '17-00085', 1, NULL, NULL),
(222, '17-00086', 1, NULL, NULL),
(223, '17-00087', 1, NULL, NULL),
(224, '17-00088', 1, NULL, NULL),
(225, '17-00089', 1, NULL, NULL),
(226, '17-00090', 1, NULL, NULL),
(227, '17-00091', 1, NULL, NULL),
(228, '17-00092', 1, NULL, NULL),
(229, '17-00093', 1, NULL, NULL),
(230, '17-00094', 1, NULL, NULL),
(231, '17-00095', 1, NULL, NULL),
(232, '17-00096', 1, NULL, NULL),
(233, '17-00097', 1, NULL, NULL),
(234, '17-00098', 1, NULL, NULL),
(235, '17-00099', 1, NULL, NULL),
(236, '17-00100', 1, NULL, NULL),
(237, '17-00101', 2, NULL, NULL),
(238, '17-00102', 2, NULL, NULL),
(239, '17-00103', 2, NULL, NULL),
(240, '17-00104', 2, NULL, NULL),
(241, '17-00105', 2, NULL, NULL),
(242, '17-00106', 2, NULL, NULL),
(243, '17-00107', 2, NULL, NULL),
(244, '17-00108', 2, NULL, NULL),
(245, '17-00109', 2, NULL, NULL),
(246, '17-00110', 2, NULL, NULL),
(247, '17-00111', 2, NULL, NULL),
(248, '17-00112', 2, NULL, NULL),
(249, '17-00113', 2, NULL, NULL),
(250, '17-00114', 2, NULL, NULL),
(251, '17-00115', 2, NULL, NULL),
(252, '17-00116', 2, NULL, NULL),
(253, '17-00117', 2, NULL, NULL),
(254, '17-00118', 2, NULL, NULL),
(255, '17-00119', 2, NULL, NULL),
(256, '17-00120', 2, NULL, NULL),
(257, '17-00121', 2, NULL, NULL),
(258, '17-00122', 2, NULL, NULL),
(259, '17-00123', 2, NULL, NULL),
(260, '17-00124', 2, NULL, NULL),
(261, '17-00125', 2, NULL, NULL),
(262, '17-00126', 2, NULL, NULL),
(263, '17-00127', 2, NULL, NULL),
(264, '17-00128', 2, NULL, NULL),
(265, '17-00129', 2, NULL, NULL),
(266, '17-00130', 2, NULL, NULL),
(267, '17-00131', 2, NULL, NULL),
(268, '17-00132', 2, NULL, NULL),
(269, '17-00133', 2, NULL, NULL),
(270, '17-00134', 2, NULL, NULL),
(271, '17-00135', 2, NULL, NULL),
(272, '17-00136', 2, NULL, NULL),
(273, '17-00137', 2, NULL, NULL),
(274, '17-00138', 2, NULL, NULL),
(275, '17-00139', 2, NULL, NULL),
(276, '17-00140', 2, NULL, NULL),
(277, '17-00141', 2, NULL, NULL),
(278, '17-00142', 2, NULL, NULL),
(279, '17-00143', 2, NULL, NULL),
(280, '17-00144', 2, NULL, NULL),
(281, '17-00145', 2, NULL, NULL),
(282, '17-00146', 2, NULL, NULL),
(283, '17-00147', 2, NULL, NULL),
(284, '17-00148', 2, NULL, NULL),
(285, '17-00149', 2, NULL, NULL),
(286, '17-00150', 2, NULL, NULL),
(287, '17-00151', 2, NULL, NULL),
(288, '17-00152', 2, NULL, NULL),
(289, '17-00153', 2, NULL, NULL),
(290, '17-00154', 2, NULL, NULL),
(291, '17-00155', 2, NULL, NULL),
(292, '17-00156', 2, NULL, NULL),
(293, '17-00157', 2, NULL, NULL),
(294, '17-00158', 2, NULL, NULL),
(295, '17-00159', 2, NULL, NULL),
(296, '17-00160', 2, NULL, NULL),
(297, '17-00161', 2, NULL, NULL),
(298, '17-00162', 2, NULL, NULL),
(299, '17-00163', 2, NULL, NULL),
(300, '17-00164', 2, NULL, NULL),
(301, '17-00165', 2, NULL, NULL),
(302, '17-00166', 2, NULL, NULL),
(303, '17-00167', 2, NULL, NULL),
(304, '17-00168', 2, NULL, NULL),
(305, '17-00169', 2, NULL, NULL),
(306, '17-00170', 2, NULL, NULL),
(307, '17-00171', 2, NULL, NULL),
(308, '17-00172', 2, NULL, NULL),
(309, '17-00173', 2, NULL, NULL),
(310, '17-00174', 2, NULL, NULL),
(311, '17-00175', 2, NULL, NULL),
(312, '17-00176', 2, NULL, NULL),
(313, '17-00177', 2, NULL, NULL),
(314, '17-00178', 2, NULL, NULL),
(315, '17-00179', 2, NULL, NULL),
(316, '17-00180', 2, NULL, NULL),
(317, '17-00181', 2, NULL, NULL),
(318, '17-00182', 2, NULL, NULL),
(319, '17-00183', 2, NULL, NULL),
(320, '17-00184', 2, NULL, NULL),
(321, '17-00185', 2, NULL, NULL),
(322, '17-00186', 2, NULL, NULL),
(323, '17-00187', 2, NULL, NULL),
(324, '17-00188', 2, NULL, NULL),
(325, '17-00189', 2, NULL, NULL),
(326, '17-00190', 2, NULL, NULL),
(327, '17-00191', 2, NULL, NULL),
(328, '17-00192', 2, NULL, NULL),
(329, '17-00193', 2, NULL, NULL),
(330, '17-00194', 2, NULL, NULL),
(331, '17-00195', 2, NULL, NULL),
(332, '17-00196', 2, NULL, NULL),
(333, '17-00197', 2, NULL, NULL),
(334, '17-00198', 2, NULL, NULL),
(335, '17-00199', 2, NULL, NULL),
(336, '17-00200', 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk_pembayaran`
--

CREATE TABLE `tb_spk_pembayaran` (
  `spkp_id` int(10) UNSIGNED NOT NULL,
  `spkp_spk` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkp_tgl` date NOT NULL,
  `spkp_jumlah` bigint(20) NOT NULL,
  `spkp_ket` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkp_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_spk_pembayaran`
--

INSERT INTO `tb_spk_pembayaran` (`spkp_id`, `spkp_spk`, `spkp_tgl`, `spkp_jumlah`, `spkp_ket`, `spkp_user`, `created_at`, `updated_at`) VALUES
(64, '17-00002', '2017-08-11', 35000000, 'DP Awal', NULL, '2017-08-11 02:54:45', '2017-08-11 02:54:45'),
(65, '17-00002', '2017-08-11', 136000000, 'pelunasan', NULL, '2017-08-11 02:57:09', '2017-08-11 02:57:09'),
(66, '17-00005', '2017-08-11', 5000000, 'DP Awal', NULL, '2017-08-11 03:46:32', '2017-08-11 03:46:32'),
(67, '17-00005', '2017-08-11', 87750000, 'pelunasan', NULL, '2017-08-11 03:47:40', '2017-08-11 03:47:40'),
(68, '17-0006', '2017-08-11', 15000000, 'DP', NULL, '2017-08-11 03:56:40', '2017-08-11 03:56:40'),
(69, '17-0006', '2017-08-11', 19900000, 'tambahan DP', NULL, '2017-08-11 04:14:39', '2017-08-11 04:14:39'),
(70, '17-00604', '2017-08-11', 40100000, 'Pembayaran DP', NULL, '2017-08-11 04:49:09', '2017-08-11 04:49:09'),
(71, '17-00604', '2017-08-11', 8910000, 'DP 2', NULL, '2017-08-11 05:01:16', '2017-08-11 05:01:16'),
(72, '17-00605', '2017-08-11', 10000000, 'DP1', NULL, '2017-08-11 05:41:23', '2017-08-11 05:41:23'),
(73, '17-00605', '2017-08-11', 40000000, 'DP ke 2', NULL, '2017-08-11 10:06:45', '2017-08-11 10:06:45'),
(74, '17-00622', '2017-08-11', 40000000, 'Pembayaran DP', NULL, '2017-08-11 10:22:59', '2017-08-11 10:22:59'),
(75, '17-01162', '2017-08-12', 35972303, 'bayar spk', NULL, '2017-08-11 21:12:27', '2017-08-11 21:12:27');

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk_referral`
--

CREATE TABLE `tb_spk_referral` (
  `spkr_id` int(10) UNSIGNED NOT NULL,
  `spkr_diskon` int(11) NOT NULL,
  `spkr_tgl` date NOT NULL,
  `spkr_ket` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk_ttbj`
--

CREATE TABLE `tb_spk_ttbj` (
  `spkt_id` int(10) UNSIGNED NOT NULL,
  `spkt_spk` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkt_biro` int(11) DEFAULT NULL,
  `spkt_tglfaktur` date DEFAULT NULL,
  `spkt_stck` text COLLATE utf8mb4_unicode_ci,
  `spkt_ujitype` date DEFAULT NULL,
  `spkt_nopol` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spkt_plat` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `spkt_notis` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `spkt_stnk` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `spkt_waktu_stnk` int(10) UNSIGNED DEFAULT NULL,
  `spkt_nobpkb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spkt_waktu_bpkb` int(10) UNSIGNED DEFAULT NULL,
  `spkt_status` int(1) NOT NULL DEFAULT '0',
  `spkt_terima_nama` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spkt_terima_alamat` text COLLATE utf8mb4_unicode_ci,
  `spkt_terima_tgl` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_spk_ttbj`
--

INSERT INTO `tb_spk_ttbj` (`spkt_id`, `spkt_spk`, `spkt_biro`, `spkt_tglfaktur`, `spkt_stck`, `spkt_ujitype`, `spkt_nopol`, `spkt_plat`, `spkt_notis`, `spkt_stnk`, `spkt_waktu_stnk`, `spkt_nobpkb`, `spkt_waktu_bpkb`, `spkt_status`, `spkt_terima_nama`, `spkt_terima_alamat`, `spkt_terima_tgl`, `created_at`, `updated_at`) VALUES
(1, '17-00002', NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2017-08-11 03:50:22', '2017-08-11 03:50:22'),
(2, '17-00005', NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2017-08-11 03:50:30', '2017-08-11 03:50:30'),
(3, '17-00622', 6, '2017-08-09', 'NOSTCK', '2017-08-31', 'BG5555UBA', 1, 1, 1, 2, '99937474', 2, 1, NULL, NULL, NULL, '2017-08-11 10:44:15', '2017-08-11 10:44:15'),
(4, '17-0006', 6, '2017-07-26', 'DDSSA', '2017-09-13', 'BG2233', 1, 1, 0, NULL, '99827374', 17, 0, NULL, NULL, NULL, '2017-08-11 10:46:22', '2017-08-11 23:17:04');

-- --------------------------------------------------------

--
-- Table structure for table `tb_stockmove`
--

CREATE TABLE `tb_stockmove` (
  `sm_id` int(10) UNSIGNED NOT NULL,
  `sm_trk` int(11) NOT NULL,
  `sm_ekspedisi` int(11) DEFAULT NULL,
  `sm_lokasi` int(11) NOT NULL,
  `sm_tglout` date DEFAULT NULL,
  `sm_tglin` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_stockmove`
--

INSERT INTO `tb_stockmove` (`sm_id`, `sm_trk`, `sm_ekspedisi`, `sm_lokasi`, `sm_tglout`, `sm_tglin`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 2, '2017-07-05', NULL, '2017-07-05 04:48:06', '2017-07-05 04:48:06'),
(4, 3, 2, 2, NULL, NULL, '2017-07-05 09:44:10', '2017-07-05 09:44:10');

-- --------------------------------------------------------

--
-- Table structure for table `tb_team`
--

CREATE TABLE `tb_team` (
  `team_id` int(10) UNSIGNED NOT NULL,
  `team_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `team_spv` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_team`
--

INSERT INTO `tb_team` (`team_id`, `team_nama`, `team_spv`, `created_at`, `updated_at`) VALUES
(1, 'Jaguar', 2, '2017-04-11 17:00:00', '2017-04-19 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_test`
--

CREATE TABLE `tb_test` (
  `id` int(11) NOT NULL,
  `val` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_test`
--

INSERT INTO `tb_test` (`id`, `val`) VALUES
(2, 'ok'),
(3, 'ok'),
(4, 'ok'),
(5, 'ok');

-- --------------------------------------------------------

--
-- Table structure for table `tb_tr_kendaraan`
--

CREATE TABLE `tb_tr_kendaraan` (
  `trk_id` int(10) UNSIGNED NOT NULL,
  `trk_ref` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trk_vendor` int(11) NOT NULL,
  `trk_invoice` bigint(20) NOT NULL,
  `trk_tgl` date NOT NULL,
  `trk_dh` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trk_indent` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trk_rrn` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trk_variantid` int(11) NOT NULL,
  `trk_warna` int(11) NOT NULL,
  `trk_tahun` int(10) UNSIGNED NOT NULL,
  `trk_rangka` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trk_mesin` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trk_dpp` bigint(20) NOT NULL,
  `trk_ekspedisi` int(11) DEFAULT NULL,
  `trk_masuk` date DEFAULT NULL,
  `trk_keluar` date DEFAULT NULL,
  `trk_lokasi` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trk_encarid` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_tr_kendaraan`
--

INSERT INTO `tb_tr_kendaraan` (`trk_id`, `trk_ref`, `trk_vendor`, `trk_invoice`, `trk_tgl`, `trk_dh`, `trk_indent`, `trk_rrn`, `trk_variantid`, `trk_warna`, `trk_tahun`, `trk_rangka`, `trk_mesin`, `trk_dpp`, `trk_ekspedisi`, `trk_masuk`, `trk_keluar`, `trk_lokasi`, `trk_encarid`, `created_at`, `updated_at`) VALUES
(28, 'P-00001', 3, 1231231, '2017-08-11', '20170001', NULL, '123131', 21, 3, 2017, 'fa22342ffa', 'dw123313321dd', 90000000, NULL, NULL, NULL, NULL, NULL, '2017-08-11 02:51:59', '2017-08-11 10:27:41'),
(29, 'P-00002', 3, 453345, '2017-08-11', '20170002', NULL, '1231124', 21, 1, 2017, 'rwe553322srr', 'tt353346de', 90000000, NULL, NULL, NULL, NULL, NULL, '2017-08-11 03:00:05', '2017-08-11 03:45:11'),
(30, 'P-00003', 4, 112331111, '2017-08-11', '20170003', NULL, '122212312', 21, 1, 2017, 'asd212qwe', '12312qwwsq1', 100000000, NULL, NULL, NULL, NULL, NULL, '2017-08-11 04:06:09', '2017-08-11 04:06:20'),
(31, 'P-00004', 3, 1929121, '2017-08-11', '20170004', NULL, '2342331', 2, 7, 2017, 'faa2342432kk', '23kkk234k24', 215000000, NULL, NULL, NULL, NULL, NULL, '2017-08-11 04:10:58', '2017-08-11 04:10:58'),
(32, 'P-00005', 3, 122234423, '2017-08-11', '20170005', NULL, '123441231', 4, 3, 2017, 'sdfwer3131wqw', '12qwe12', 500000000, NULL, NULL, NULL, NULL, NULL, '2017-08-11 04:50:31', '2017-08-11 04:50:31'),
(33, 'P-00006', 3, 9928, '2017-08-11', '20170006', NULL, '123123', 2, 5, 2017, '123123', '123123123', 190000000, NULL, NULL, NULL, NULL, NULL, '2017-08-11 05:49:09', '2017-08-11 05:49:09'),
(34, 'P-00007', 3, 20001, '2017-08-11', '20170007', NULL, '322323', 21, 3, 2017, '434234', '1231231', 150000000, NULL, NULL, NULL, NULL, NULL, '2017-08-11 10:26:25', '2017-08-11 10:26:54'),
(35, 'P-00008', 3, 290008, '2017-08-11', '20170008', NULL, '12233133', 28, 3, 2017, '22350009', '5569567790', 150000000, NULL, NULL, NULL, NULL, NULL, '2017-08-11 10:28:29', '2017-08-11 10:28:29'),
(36, 'P-00009', 3, 8272727, '2017-08-12', '20170009', NULL, '63636', 25, 8, 2017, '36364', '3747347', 150000000, NULL, NULL, NULL, NULL, NULL, '2017-08-11 22:32:21', '2017-08-11 22:32:21');

-- --------------------------------------------------------

--
-- Table structure for table `tb_type`
--

CREATE TABLE `tb_type` (
  `type_id` int(10) UNSIGNED NOT NULL,
  `type_nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_poin` int(11) NOT NULL,
  `type_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_type`
--

INSERT INTO `tb_type` (`type_id`, `type_nama`, `type_poin`, `type_status`, `created_at`, `updated_at`) VALUES
(2, 'XENIA', 9, 1, NULL, NULL),
(3, 'TERIOS', 7, 1, NULL, NULL),
(4, 'GM PU 1.5', 5, 1, NULL, NULL),
(5, 'SIGRA', 9, 1, NULL, NULL),
(6, 'AYLA', 7, 1, NULL, NULL),
(7, 'HI-MAX', 6, 1, NULL, NULL),
(8, 'GM PU 1.3', 8, 1, NULL, NULL),
(9, 'GM FAN 1.3', 10, 1, NULL, NULL),
(10, 'GM FAN 1.5', 8, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_users`
--

CREATE TABLE `tb_users` (
  `nip` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_users`
--

INSERT INTO `tb_users` (`nip`, `nama`, `password`, `created_at`, `updated_at`) VALUES
('101', 'Admin', '$2y$10$DcG6hy6UHlyhyvUCaDFL3.L3LU8halUxVBDghJ.KWHnbexTUvV8Ki', '2017-04-10 02:13:00', '2017-04-10 02:13:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_variant`
--

CREATE TABLE `tb_variant` (
  `variant_id` int(11) NOT NULL,
  `variant_serial` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `variant_type` int(11) NOT NULL,
  `variant_nama` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `variant_ket` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `variant_off` bigint(20) NOT NULL DEFAULT '0',
  `variant_on` bigint(20) NOT NULL DEFAULT '0',
  `variant_bbn` bigint(20) NOT NULL DEFAULT '0',
  `variant_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_variant`
--

INSERT INTO `tb_variant` (`variant_id`, `variant_serial`, `variant_type`, `variant_nama`, `variant_ket`, `variant_off`, `variant_on`, `variant_bbn`, `variant_status`, `created_at`, `updated_at`) VALUES
(1, 'F650RV-GMDF', 2, 'M MT 1.0', 'XENIA FMC M MT 1.0', 0, 108900000, 2100000, 1, '2017-04-17 22:19:33', '2017-08-11 03:40:52'),
(2, 'F700RGGMRFJE', 3, 'MT EXTRA', 'TERIOS X MT EXTRA', 0, 244000000, 2000000, 1, '2017-07-19 21:45:27', '2017-08-11 03:40:22'),
(3, 'F653MD-R-SPT', 2, 'MT 1.3 SPORTY', 'GREAT NEW XENIA R MT 1.3 SPORTY', 0, 204100000, 2000000, 1, '2017-07-19 23:12:59', '2017-08-11 03:42:12'),
(4, 'B401RS-GMZFJG', 5, '1.2 R MT', 'SIGRA 1.2 R MT', 0, 184750000, 2000000, 1, '2017-07-19 23:14:25', '2017-08-11 03:38:22'),
(5, 'B401RS-GQZFJH', 5, '1.2 R AT', 'SIGRA 1.2 R AT', 0, 132650000, 2000000, 1, '2017-07-19 23:15:39', '2017-08-11 03:38:06'),
(6, 'B401RS-GMQFJW', 5, '1.2 X MT', 'SIGRA 1.2 X MT', 0, 132650000, 1500000, 1, '2017-07-19 23:17:20', '2017-08-11 03:38:49'),
(7, 'B400RS-GMDEJW', 5, '1.0 M MT', 'SIGRA 1.0 M MT', 0, 117800000, 1000000, 1, '2017-07-19 23:19:16', '2017-08-11 03:37:17'),
(8, 'F653MR-XJ', 2, 'X MT 1.3 STD', 'GREAT NEW XENIA X MT 1.3 STD', 0, 195700000, 23000000, 1, '2017-07-19 23:20:21', '2017-08-11 03:42:51'),
(9, 'F653MD-RE', 2, 'R MT 1.3', 'GREAT NEW XENIA R MT 1.3', 0, 184750000, 20000000, 1, '2017-07-19 23:21:59', '2017-08-11 03:41:54'),
(10, 'F700RGGMRFJOE', 3, 'X AT EXTRA', 'TERIOS X AT EXTRA', 0, 216000000, 23000000, 1, '2017-07-19 23:25:36', '2017-08-11 03:40:13'),
(11, 'F700RGGMRFJSE', 3, 'STD MT EXTRA', 'TERIOS STD MT EXTRA', 0, 205500000, 20000000, 1, '2017-07-19 23:26:46', '2017-08-11 03:39:59'),
(13, 'F700RGGADFJSE', 3, 'STD MT', 'TERIOS STD MT', 0, 195200000, 21000000, 1, '2017-07-19 23:28:05', '2017-08-11 03:39:46'),
(14, 'F700RGGFGFJS', 3, 'STD AT', 'TERIOS STD AT', 0, 216000000, 21000000, 1, '2017-07-19 23:28:42', '2017-08-11 03:39:23'),
(15, 'F653AR-X', 2, 'X AT 1.3 STD', 'GREAT NEW XENIA X AT 1.3 STD', 0, 191300000, 17000000, 1, '2017-07-19 23:29:27', '2017-08-11 03:42:27'),
(16, 'F653AD-R', 2, 'R AT 1.3', 'GREAT NEW XENIA R AT 1.3', 0, 195000000, 18000000, 1, '2017-07-19 23:30:47', '2017-08-11 03:41:23'),
(17, 'GMRP-PMRFJJ-KJFH', 4, 'GM PU 1.5 AC PS 1.5 FH', 'GRAN MAX PU AC PS 1.5 FH', 0, 146300000, 17000000, 1, '2017-07-19 23:31:52', '2017-08-11 03:36:21'),
(18, 'GMRP-TMREJJ-HCFH', 8, '3W FH', 'GRAND MAX PU 1.3 3W FH', 0, 134600000, 13000000, 1, '2017-07-19 23:36:18', '2017-08-11 03:36:06'),
(20, 'GMRF-TMREJJ-HCFH', 9, '3W FH', 'GRAND MAX FAN 1.3 3W FH', 0, 115200000, 17000000, 1, '2017-07-19 23:43:24', '2017-08-11 03:35:19'),
(22, 'AYLA-GADFJ-PN', 6, '1.2 M AT MI', 'AYLA 1.2 M AT MI', 0, 125450000, 11000000, 1, '2017-07-19 23:52:05', '2017-08-11 03:33:53'),
(23, 'AYLA-GADFJ-LK', 6, '1.0 M AT MI', 'AYLA 1.0 M AT MI', 0, 118050000, 20000000, 1, '2017-07-19 23:55:29', '2017-08-11 03:33:22'),
(24, 'AYLA-GADFJ-GK', 6, '1.0 M MT MI', 'AYLA 1.0 M MT MI', 0, 92550000, 10000000, 1, '2017-07-19 23:57:15', '2017-08-11 03:32:32'),
(25, 'AYLA-GMDFJ-S', 6, '1.2 M MT SPORTY', 'AYLA 1.2 M MT SPORTY', 0, 125250000, 10000000, 1, '2017-07-19 23:58:27', '2017-08-11 03:31:53'),
(26, 'AYLA-GMDFJ-S', 6, '1.2 M AT SPORTY', 'AYLA 1.2 M AT SPORTY', 0, 123750000, 10000000, 1, '2017-07-19 23:59:48', '2017-08-11 03:34:15'),
(27, 'S501RP-PMRFJ-YD', 7, 'PU 1.0 STD', 'HI MAX PU 1.0 STD', 0, 96550000, 0, 1, '2017-07-20 00:00:36', '2017-08-11 03:36:56'),
(28, 'S501RP-PMRFJ-BH', 7, '1.0 STD AC PS', 'HI MAX PU 1.0 STD AC PS', 0, 104550000, 0, 1, '2017-07-20 00:01:05', '2017-08-11 03:36:49'),
(34, 'AYLA-GMDFJ-PN', 6, '1.2 M MT MI', 'AYLA 1.2 M MT MI', 0, 0, 0, 1, NULL, NULL),
(35, 'F700RGGMRWJS', 3, 'STD AT EXTRA', 'TERIOS STD AT EXTRA', 0, 0, 0, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_vendor`
--

CREATE TABLE `tb_vendor` (
  `vendor_id` int(10) UNSIGNED NOT NULL,
  `vendor_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendor_alamat` text COLLATE utf8mb4_unicode_ci,
  `vendor_kota` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendor_kodepos` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_notelp` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendor_email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_vendor`
--

INSERT INTO `tb_vendor` (`vendor_id`, `vendor_nama`, `vendor_alamat`, `vendor_kota`, `vendor_kodepos`, `vendor_notelp`, `vendor_email`, `created_at`, `updated_at`) VALUES
(3, 'PT. Encar Daihatsu', 'Jakarta Pusat', 'Jakarta', '30112', '218119119', 'encardaihatsu@daihatsu.com', '2017-08-11 02:50:49', '2017-08-11 03:26:09'),
(4, 'PT. Karya Siber Indonesia', 'Palembang Pusat', 'Palembang', '30163', '711877721', 'karyasiber@gmail.com', '2017-08-11 03:26:49', '2017-08-11 03:26:49');

-- --------------------------------------------------------

--
-- Table structure for table `tb_vendoraks`
--

CREATE TABLE `tb_vendoraks` (
  `vendorAks_id` int(10) UNSIGNED NOT NULL,
  `vendorAks_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendorAks_alamat` text COLLATE utf8mb4_unicode_ci,
  `vendorAks_kota` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendorAks_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendorAks_email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendorAks_kodepos` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendorAks_status` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_vendoraks`
--

INSERT INTO `tb_vendoraks` (`vendorAks_id`, `vendorAks_nama`, `vendorAks_alamat`, `vendorAks_kota`, `vendorAks_telp`, `vendorAks_email`, `vendorAks_kodepos`, `vendorAks_status`, `created_at`, `updated_at`) VALUES
(2, 'PT. AKSESORIS INDONESIA', 'Jl. Panjaitan kedua', 'Jakarta', '021999921', 'support@aksesoris.co.id', '100022', 1, '2017-08-11 03:28:24', '2017-08-11 03:28:24'),
(3, 'PT. NOTIFIKASI AKSESORIS', 'Jl. KMS ALI no.23', 'Palembang', '0711877727', 'cs@notifikasi.co.id', '30990', 1, '2017-08-11 03:30:36', '2017-08-11 03:30:36');

-- --------------------------------------------------------

--
-- Table structure for table `tb_warna`
--

CREATE TABLE `tb_warna` (
  `warna_id` int(10) UNSIGNED NOT NULL,
  `warna_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `warna_type` int(11) NOT NULL,
  `warna_status` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_warna`
--

INSERT INTO `tb_warna` (`warna_id`, `warna_nama`, `warna_type`, `warna_status`, `created_at`, `updated_at`) VALUES
(0, 'BEBAS', 2, 1, NULL, NULL),
(1, 'RED DSO', 2, 1, NULL, NULL),
(2, 'BLACK DSO', 2, 1, NULL, NULL),
(3, 'BLUE DSO', 2, 1, NULL, NULL),
(4, 'RED SOLID', 2, 1, NULL, NULL),
(5, 'RED METALIC', 2, 1, NULL, NULL),
(6, 'BLUE METALIC', 2, 1, NULL, NULL),
(7, 'GREY', 2, 1, NULL, NULL),
(8, 'WHITE DSO', 2, 1, NULL, NULL),
(9, 'BLACK METALIC', 2, 1, NULL, NULL),
(21, 'RED DSO', 3, 1, NULL, NULL),
(22, 'BLACK DSO', 3, 1, NULL, NULL),
(23, 'BLUE DSO', 3, 1, NULL, NULL),
(24, 'RED SOLID', 3, 1, NULL, NULL),
(25, 'RED METALIC', 3, 1, NULL, NULL),
(26, 'BLUE METALIC', 3, 1, NULL, NULL),
(27, 'GREY', 3, 1, NULL, NULL),
(28, 'WHITE DSO', 3, 1, NULL, NULL),
(29, 'BLACK METALIC', 3, 1, NULL, NULL),
(30, 'RED DSO', 4, 1, NULL, NULL),
(31, 'BLACK DSO', 4, 1, NULL, NULL),
(32, 'BLUE DSO', 4, 1, NULL, NULL),
(33, 'RED SOLID', 4, 1, NULL, NULL),
(34, 'RED METALIC', 4, 1, NULL, NULL),
(35, 'BLUE METALIC', 4, 1, NULL, NULL),
(36, 'GREY', 4, 1, NULL, NULL),
(37, 'WHITE DSO', 4, 1, NULL, NULL),
(38, 'BLACK METALIC', 4, 1, NULL, NULL),
(39, 'RED DSO', 5, 1, NULL, NULL),
(40, 'BLACK DSO', 5, 1, NULL, NULL),
(41, 'BLUE DSO', 5, 1, NULL, NULL),
(42, 'RED SOLID', 5, 1, NULL, NULL),
(43, 'RED METALIC', 5, 1, NULL, NULL),
(44, 'BLUE METALIC', 5, 1, NULL, NULL),
(45, 'GREY', 5, 1, NULL, NULL),
(46, 'WHITE DSO', 5, 1, NULL, NULL),
(47, 'BLACK METALIC', 5, 1, NULL, NULL),
(48, 'RED DSO', 6, 1, NULL, NULL),
(49, 'BLACK DSO', 6, 1, NULL, NULL),
(50, 'BLUE DSO', 6, 1, NULL, NULL),
(51, 'RED SOLID', 6, 1, NULL, NULL),
(52, 'RED METALIC', 6, 1, NULL, NULL),
(53, 'BLUE METALIC', 6, 1, NULL, NULL),
(54, 'GREY', 6, 1, NULL, NULL),
(55, 'WHITE DSO', 6, 1, NULL, NULL),
(56, 'BLACK METALIC', 6, 1, NULL, NULL),
(57, 'RED DSO', 7, 1, NULL, NULL),
(58, 'BLACK DSO', 7, 1, NULL, NULL),
(59, 'BLUE DSO', 7, 1, NULL, NULL),
(60, 'RED SOLID', 7, 1, NULL, NULL),
(61, 'RED METALIC', 7, 1, NULL, NULL),
(62, 'BLUE METALIC', 7, 1, NULL, NULL),
(63, 'GREY', 7, 1, NULL, NULL),
(64, 'WHITE DSO', 7, 1, NULL, NULL),
(65, 'BLACK METALIC', 7, 1, NULL, NULL),
(66, 'RED DSO', 8, 1, NULL, NULL),
(67, 'BLACK DSO', 8, 1, NULL, NULL),
(68, 'BLUE DSO', 8, 1, NULL, NULL),
(69, 'RED SOLID', 8, 1, NULL, NULL),
(70, 'RED METALIC', 8, 1, NULL, NULL),
(71, 'BLUE METALIC', 8, 1, NULL, NULL),
(72, 'GREY', 8, 1, NULL, NULL),
(73, 'WHITE DSO', 8, 1, NULL, NULL),
(74, 'BLACK METALIC', 8, 1, NULL, NULL),
(75, 'RED DSO', 9, 1, NULL, NULL),
(76, 'BLACK DSO', 9, 1, NULL, NULL),
(77, 'BLUE DSO', 9, 1, NULL, NULL),
(78, 'RED SOLID', 9, 1, NULL, NULL),
(79, 'RED METALIC', 9, 1, NULL, NULL),
(80, 'BLUE METALIC', 9, 1, NULL, NULL),
(81, 'GREY', 9, 1, NULL, NULL),
(82, 'WHITE DSO', 9, 1, NULL, NULL),
(83, 'BLACK METALIC', 9, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_referral_status`
-- (See below for the actual view)
--
CREATE TABLE `vw_referral_status` (
`spkd_referral` int(11)
,`spkd_jumlah` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_spk`
-- (See below for the actual view)
--
CREATE TABLE `vw_spk` (
`spk_id` varchar(10)
,`spk_tgl` date
,`karyawan_nama` varchar(50)
,`spk_pel_nama` varchar(50)
,`spk_pel_kota` varchar(30)
,`spk_dh` varchar(10)
,`spk_match` date
,`spk_waktu_match` date
,`spk_status` char(2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_stock_kendaraan`
-- (See below for the actual view)
--
CREATE TABLE `vw_stock_kendaraan` (
`trk_id` int(10) unsigned
,`trk_ref` varchar(10)
,`trk_vendor` int(11)
,`trk_invoice` bigint(20)
,`trk_tgl` date
,`trk_dh` varchar(10)
,`trk_indent` varchar(15)
,`trk_rrn` varchar(30)
,`trk_variantid` int(11)
,`trk_warna` int(11)
,`trk_tahun` int(10) unsigned
,`trk_rangka` varchar(25)
,`trk_mesin` varchar(25)
,`trk_dpp` bigint(20)
,`trk_ekspedisi` int(11)
,`trk_masuk` date
,`trk_lokasi` varchar(50)
,`trk_encarid` varchar(20)
,`created_at` timestamp
,`updated_at` timestamp
);

-- --------------------------------------------------------

--
-- Structure for view `vw_referral_status`
--
DROP TABLE IF EXISTS `vw_referral_status`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_referral_status`  AS  select `tb_spk_diskon`.`spkd_referral` AS `spkd_referral`,count(0) AS `spkd_jumlah` from (`tb_spk_diskon` join `tb_spk` on(((convert(`tb_spk`.`spk_id` using utf8mb4) = `tb_spk_diskon`.`spkd_spk`) and (`tb_spk`.`spk_do` <> NULL)))) where (`tb_spk_diskon`.`spkd_status` = 1) group by `tb_spk_diskon`.`spkd_referral` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_spk`
--
DROP TABLE IF EXISTS `vw_spk`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_spk`  AS  select `tb_spk`.`spk_id` AS `spk_id`,`tb_spk`.`spk_tgl` AS `spk_tgl`,`tb_karyawan`.`karyawan_nama` AS `karyawan_nama`,`tb_spk`.`spk_pel_nama` AS `spk_pel_nama`,`tb_spk`.`spk_pel_kota` AS `spk_pel_kota`,`tb_spk`.`spk_dh` AS `spk_dh`,`tb_spk`.`spk_match` AS `spk_match`,`tb_spk`.`spk_waktu_match` AS `spk_waktu_match`,`tb_spk`.`spk_status` AS `spk_status` from ((`tb_spk` join `tb_sales` on((`tb_spk`.`spk_sales` = `tb_sales`.`sales_id`))) join `tb_karyawan` on((`tb_sales`.`sales_karyawan` = `tb_karyawan`.`karyawan_id`))) where (`tb_spk`.`spk_status` = 2) order by `tb_spk`.`spk_tgl` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_stock_kendaraan`
--
DROP TABLE IF EXISTS `vw_stock_kendaraan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_stock_kendaraan`  AS  select `tb_tr_kendaraan`.`trk_id` AS `trk_id`,`tb_tr_kendaraan`.`trk_ref` AS `trk_ref`,`tb_tr_kendaraan`.`trk_vendor` AS `trk_vendor`,`tb_tr_kendaraan`.`trk_invoice` AS `trk_invoice`,`tb_tr_kendaraan`.`trk_tgl` AS `trk_tgl`,`tb_tr_kendaraan`.`trk_dh` AS `trk_dh`,`tb_tr_kendaraan`.`trk_indent` AS `trk_indent`,`tb_tr_kendaraan`.`trk_rrn` AS `trk_rrn`,`tb_tr_kendaraan`.`trk_variantid` AS `trk_variantid`,`tb_tr_kendaraan`.`trk_warna` AS `trk_warna`,`tb_tr_kendaraan`.`trk_tahun` AS `trk_tahun`,`tb_tr_kendaraan`.`trk_rangka` AS `trk_rangka`,`tb_tr_kendaraan`.`trk_mesin` AS `trk_mesin`,`tb_tr_kendaraan`.`trk_dpp` AS `trk_dpp`,`tb_tr_kendaraan`.`trk_ekspedisi` AS `trk_ekspedisi`,`tb_tr_kendaraan`.`trk_masuk` AS `trk_masuk`,`tb_tr_kendaraan`.`trk_lokasi` AS `trk_lokasi`,`tb_tr_kendaraan`.`trk_encarid` AS `trk_encarid`,`tb_tr_kendaraan`.`created_at` AS `created_at`,`tb_tr_kendaraan`.`updated_at` AS `updated_at` from (`tb_tr_kendaraan` left join `tb_spk` on((convert(`tb_spk`.`spk_dh` using utf8mb4) = `tb_tr_kendaraan`.`trk_dh`))) where isnull(`tb_spk`.`spk_dh`) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_aksesoris`
--
ALTER TABLE `tb_aksesoris`
  ADD PRIMARY KEY (`aksesoris_id`);

--
-- Indexes for table `tb_asuransi`
--
ALTER TABLE `tb_asuransi`
  ADD PRIMARY KEY (`asuransi_id`);

--
-- Indexes for table `tb_asuransi_jenis`
--
ALTER TABLE `tb_asuransi_jenis`
  ADD PRIMARY KEY (`ajenis_id`);

--
-- Indexes for table `tb_bank`
--
ALTER TABLE `tb_bank`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `tb_bank_kategori`
--
ALTER TABLE `tb_bank_kategori`
  ADD PRIMARY KEY (`kategori_id`);

--
-- Indexes for table `tb_biro`
--
ALTER TABLE `tb_biro`
  ADD PRIMARY KEY (`biro_id`);

--
-- Indexes for table `tb_cmo`
--
ALTER TABLE `tb_cmo`
  ADD PRIMARY KEY (`cmo_id`);

--
-- Indexes for table `tb_config`
--
ALTER TABLE `tb_config`
  ADD PRIMARY KEY (`config_id`);

--
-- Indexes for table `tb_ekspedisi`
--
ALTER TABLE `tb_ekspedisi`
  ADD PRIMARY KEY (`ekspedisi_id`);

--
-- Indexes for table `tb_gudang`
--
ALTER TABLE `tb_gudang`
  ADD PRIMARY KEY (`gudang_id`);

--
-- Indexes for table `tb_jabatan`
--
ALTER TABLE `tb_jabatan`
  ADD PRIMARY KEY (`jabatan_id`);

--
-- Indexes for table `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  ADD PRIMARY KEY (`karyawan_id`);

--
-- Indexes for table `tb_leasing`
--
ALTER TABLE `tb_leasing`
  ADD PRIMARY KEY (`leasing_id`);

--
-- Indexes for table `tb_leasing_bayar`
--
ALTER TABLE `tb_leasing_bayar`
  ADD PRIMARY KEY (`lbayar_id`);

--
-- Indexes for table `tb_pelanggan`
--
ALTER TABLE `tb_pelanggan`
  ADD PRIMARY KEY (`pel_id`);

--
-- Indexes for table `tb_referral`
--
ALTER TABLE `tb_referral`
  ADD PRIMARY KEY (`referral_id`);

--
-- Indexes for table `tb_sales`
--
ALTER TABLE `tb_sales`
  ADD PRIMARY KEY (`sales_id`);

--
-- Indexes for table `tb_spk`
--
ALTER TABLE `tb_spk`
  ADD PRIMARY KEY (`spk_id`);

--
-- Indexes for table `tb_spk_diskon`
--
ALTER TABLE `tb_spk_diskon`
  ADD PRIMARY KEY (`spkd_id`);

--
-- Indexes for table `tb_spk_faktur`
--
ALTER TABLE `tb_spk_faktur`
  ADD PRIMARY KEY (`spkf_id`);

--
-- Indexes for table `tb_spk_leasing`
--
ALTER TABLE `tb_spk_leasing`
  ADD PRIMARY KEY (`spkl_id`);

--
-- Indexes for table `tb_spk_no`
--
ALTER TABLE `tb_spk_no`
  ADD PRIMARY KEY (`spkNo_id`);

--
-- Indexes for table `tb_spk_pembayaran`
--
ALTER TABLE `tb_spk_pembayaran`
  ADD PRIMARY KEY (`spkp_id`);

--
-- Indexes for table `tb_spk_referral`
--
ALTER TABLE `tb_spk_referral`
  ADD PRIMARY KEY (`spkr_id`);

--
-- Indexes for table `tb_spk_ttbj`
--
ALTER TABLE `tb_spk_ttbj`
  ADD PRIMARY KEY (`spkt_id`);

--
-- Indexes for table `tb_stockmove`
--
ALTER TABLE `tb_stockmove`
  ADD PRIMARY KEY (`sm_id`);

--
-- Indexes for table `tb_team`
--
ALTER TABLE `tb_team`
  ADD PRIMARY KEY (`team_id`);

--
-- Indexes for table `tb_test`
--
ALTER TABLE `tb_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_tr_kendaraan`
--
ALTER TABLE `tb_tr_kendaraan`
  ADD PRIMARY KEY (`trk_id`);

--
-- Indexes for table `tb_type`
--
ALTER TABLE `tb_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `tb_users`
--
ALTER TABLE `tb_users`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `tb_variant`
--
ALTER TABLE `tb_variant`
  ADD PRIMARY KEY (`variant_id`);

--
-- Indexes for table `tb_vendor`
--
ALTER TABLE `tb_vendor`
  ADD PRIMARY KEY (`vendor_id`);

--
-- Indexes for table `tb_vendoraks`
--
ALTER TABLE `tb_vendoraks`
  ADD PRIMARY KEY (`vendorAks_id`);

--
-- Indexes for table `tb_warna`
--
ALTER TABLE `tb_warna`
  ADD PRIMARY KEY (`warna_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `tb_aksesoris`
--
ALTER TABLE `tb_aksesoris`
  MODIFY `aksesoris_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `tb_asuransi`
--
ALTER TABLE `tb_asuransi`
  MODIFY `asuransi_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_asuransi_jenis`
--
ALTER TABLE `tb_asuransi_jenis`
  MODIFY `ajenis_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_bank`
--
ALTER TABLE `tb_bank`
  MODIFY `bank_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_bank_kategori`
--
ALTER TABLE `tb_bank_kategori`
  MODIFY `kategori_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_biro`
--
ALTER TABLE `tb_biro`
  MODIFY `biro_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_cmo`
--
ALTER TABLE `tb_cmo`
  MODIFY `cmo_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_config`
--
ALTER TABLE `tb_config`
  MODIFY `config_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tb_ekspedisi`
--
ALTER TABLE `tb_ekspedisi`
  MODIFY `ekspedisi_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_gudang`
--
ALTER TABLE `tb_gudang`
  MODIFY `gudang_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_jabatan`
--
ALTER TABLE `tb_jabatan`
  MODIFY `jabatan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  MODIFY `karyawan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_leasing`
--
ALTER TABLE `tb_leasing`
  MODIFY `leasing_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tb_leasing_bayar`
--
ALTER TABLE `tb_leasing_bayar`
  MODIFY `lbayar_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_pelanggan`
--
ALTER TABLE `tb_pelanggan`
  MODIFY `pel_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `tb_referral`
--
ALTER TABLE `tb_referral`
  MODIFY `referral_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_sales`
--
ALTER TABLE `tb_sales`
  MODIFY `sales_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_spk_diskon`
--
ALTER TABLE `tb_spk_diskon`
  MODIFY `spkd_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `tb_spk_leasing`
--
ALTER TABLE `tb_spk_leasing`
  MODIFY `spkl_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_spk_no`
--
ALTER TABLE `tb_spk_no`
  MODIFY `spkNo_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=337;
--
-- AUTO_INCREMENT for table `tb_spk_pembayaran`
--
ALTER TABLE `tb_spk_pembayaran`
  MODIFY `spkp_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `tb_spk_referral`
--
ALTER TABLE `tb_spk_referral`
  MODIFY `spkr_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_spk_ttbj`
--
ALTER TABLE `tb_spk_ttbj`
  MODIFY `spkt_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_stockmove`
--
ALTER TABLE `tb_stockmove`
  MODIFY `sm_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_team`
--
ALTER TABLE `tb_team`
  MODIFY `team_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_test`
--
ALTER TABLE `tb_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_tr_kendaraan`
--
ALTER TABLE `tb_tr_kendaraan`
  MODIFY `trk_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `tb_type`
--
ALTER TABLE `tb_type`
  MODIFY `type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `tb_variant`
--
ALTER TABLE `tb_variant`
  MODIFY `variant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tb_vendor`
--
ALTER TABLE `tb_vendor`
  MODIFY `vendor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_vendoraks`
--
ALTER TABLE `tb_vendoraks`
  MODIFY `vendorAks_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_warna`
--
ALTER TABLE `tb_warna`
  MODIFY `warna_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
